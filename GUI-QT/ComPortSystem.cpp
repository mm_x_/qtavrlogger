/*
 * ComPortSystem.cpp
 *
 *  Created on: 27.12.2011
 *      Author: user
 */

#include <ComPortSystem.h>
#include <qavrlogger.h>

ComPortSystem::ComPortSystem(qAVRLogger * parent) :
		m_pParent(parent), m_isConnect(false)
{
	m_pControl = esSInit();
}

ComPortSystem::~ComPortSystem()
{

}

bool ComPortSystem::connect(const char *path)
{
	disconnect();

	char * path_copy = new char[strlen(path) + 1];
	strcpy(path_copy, path);
	m_isConnect = esSOpen(m_pControl, path_copy) == 0;
	delete path_copy;

	esSSetBaudRate(m_pControl, 115200);
	esSSetCharSize(m_pControl, 8);
	esSSetFlow(m_pControl, esSFlowNone);
	esSSetParity(m_pControl, esSParityNone);
	esSSetStop(m_pControl, esSStopOne);

	return m_isConnect;
}

void ComPortSystem::disconnect()
{
	m_isConnect = false;
	QMutexLocker lock(&m_mutexRun);
	esSClose(m_pControl);
}

bool ComPortSystem::isConnect()
{
	return m_isConnect;
}

char *ComPortSystem::getLastErrorMsg()
{
	if (!m_pControl)
		return "Not init";

	return esSGetLastErrorMsg(m_pControl);
}

void ComPortSystem::run()
{
	QMutexLocker lock(&m_mutexRun);
	int attempts = 0;

	esSetCallBackReceiveFreq(m_pControl, putFreq, this);
	esSetCallBackReceiveADC(m_pControl, putADC, this);
	esSetCallBackReceiveCommand(m_pControl, putCommand, this);

	if (isConnect())
	{
		esSSendEnableFreq(m_pControl, 0, true);
		esSSendEnableADC(m_pControl, 0, true);
		esSSendEnableADC(m_pControl, 1, true);
		esSSendEnableADC(m_pControl, 2, true);
		esSSendEnableADC(m_pControl, 3, true);
		esSSendEnableADC(m_pControl, 4, true);
		esSSendEnableADC(m_pControl, 5, true);
		esSSendEnableADC(m_pControl, 6, true);
		esSSendEnableADC(m_pControl, 7, true);

	}

	while (isConnect())
	{
		int res = 0;
		esSSendGetFreq(m_pControl, 0, esSNotNorm, esSFloat);
		msleep(20);

		if (esSUpdate(m_pControl, 20) == 1)
			res++;

		for (int ch = 0; ch < 8; ch++)
		{
			esSSendGetADC(m_pControl, ch, esSNotNorm, esSWord);
			msleep(20);

			if (esSUpdate(m_pControl, 20) == 1)
				res++;
		}

		/*if (res == 1)
		 Log::info("ESUART: get failed res - 1", res);*/

		if (res >= 1)
		{
			attempts = 0;
		}
		else
		{
			/*Log::warning("ESUART: get failed res - 0");*/
			attempts++;
		}

		if (attempts > 10)
		{
			/*Log::error("ESUART: get failed - attempts > 10");*/
			m_isConnect = false;
			esSClose(m_pControl);
		}
	}
}

void ComPortSystem::putCommand(struct esSerialControl *control, int cmd, void *buff, int type_arg, void *client)
{
}

void ComPortSystem::putFreq(struct esSerialControl *control, int channel, int is_norm, void *buff, int type_arg,
		void *client)
{
	ComPortSystem * obj = static_cast<ComPortSystem *>(client);

	float freq = 0;
	switch (type_arg)
	{
	case esSByte:
		freq = *(int8_t*) buff;
		break;
	case esSWord:
		freq = *(int16_t *) buff;
		break;
	case esSFloat:
		freq = *(float *) buff;
		break;
	default:
		break;
	}

	obj->m_pParent->put_freq(freq);
}

void ComPortSystem::putADC(struct esSerialControl *control, int channel, int is_norm, void *buff, int type_arg,
		void *client)
{
	ComPortSystem * obj = static_cast<ComPortSystem *>(client);

	float val = 0;
	switch (type_arg)
	{
	case esSByte:
		val = *(int8_t*) buff;
		break;
	case esSWord:
		val = *(int16_t *) buff;
		break;
	case esSFloat:
		val = *(float *) buff;
		break;
	default:
		break;
	}

	if (channel >= 0 && channel < 8)
		obj->m_pParent->put_ADC(val, channel);
}

