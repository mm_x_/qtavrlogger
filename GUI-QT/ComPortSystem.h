/*
 * ComPortSystem.h
 *
 *  Created on: 27.12.2011
 *      Author: user
 */

#ifndef COMPORTSYSTEM_H_
#define COMPORTSYSTEM_H_

#include <QtCore/qobject.h>
#include <QtCore/qthread.h>
#include <QtCore/qmutex.h>

#include <esSerialPort.h>

#include <enginesound/BaseObject.h>

class qAVRLogger;

class ComPortSystem: public QThread
{
public:
	ComPortSystem(qAVRLogger * parent);
	virtual ~ComPortSystem();

	bool connect(const char * path);
	void disconnect();
	bool isConnect();

	char * getLastErrorMsg();
protected:
	void run();

	static void putCommand(struct esSerialControl* control, int cmd, void * buff, int type_arg, void * client);
	static void putFreq(struct esSerialControl*control, int channel, int is_norm, void * buff, int type_arg,
			void * client);
	static void putADC(struct esSerialControl*control, int channel, int is_norm, void * buff, int type_arg,
			void * client);

private:
	qAVRLogger * m_pParent;

	bool m_isConnect;
	esSerialControl * m_pControl;

	QMutex m_mutexRun;
};

#endif /* COMPORTSYSTEM_H_ */
