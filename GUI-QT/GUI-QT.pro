#-------------------------------------------------
#
# Project created by QtCreator 2013-05-15T14:41:28
#
#-------------------------------------------------

QT       += core gui

TARGET = QT-GUI
TEMPLATE = app
INCLUDEPATH += $$PWD/../library/esLibrary/include
INCLUDEPATH += $$PWD/../library/esSerialLibrary/include


SOURCES += main.cpp\
        qavrlogger.cpp \
    ComPortSystem.cpp \
    dialogsettingsthermal.cpp

HEADERS  += qavrlogger.h \
    ComPortSystem.h \
    dialogsettingsthermal.h

FORMS    += qavrlogger.ui \
    dialogsettingsthermal.ui

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../library/esLibrary/release/ -lesLibrary
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../library/esLibrary/debug/ -lesLibrary
else:symbian: LIBS += -lesLibrary
else:unix: LIBS += -L$$OUT_PWD/../library/esLibrary/ -lesLibrary

INCLUDEPATH += $$PWD/../library/esLibrary
DEPENDPATH += $$PWD/../library/esLibrary

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../library/esLibrary/release/esLibrary.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../library/esLibrary/debug/esLibrary.lib
else:unix:!symbian: PRE_TARGETDEPS += $$OUT_PWD/../library/esLibrary/libesLibrary.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../library/esSerialLibrary/release/ -lesSerialLibrary
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../library/esSerialLibrary/debug/ -lesSerialLibrary
else:symbian: LIBS += -lesSerialLibrary
else:unix: LIBS += -L$$OUT_PWD/../library/esSerialLibrary/ -lesSerialLibrary

INCLUDEPATH += $$PWD/../library/esSerialLibrary
DEPENDPATH += $$PWD/../library/esSerialLibrary

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../library/esSerialLibrary/release/esSerialLibrary.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../library/esSerialLibrary/debug/esSerialLibrary.lib
else:unix:!symbian: PRE_TARGETDEPS += $$OUT_PWD/../library/esSerialLibrary/libesSerialLibrary.a
