#include "dialogsettingsthermal.h"
#include "ui_dialogsettingsthermal.h"

DialogSettingsThermal::DialogSettingsThermal(QWidget *parent) :
		QDialog(parent), ui(new Ui::DialogSettingsThermal)
{
	ui->setupUi(this);
}

DialogSettingsThermal::~DialogSettingsThermal()
{
	delete ui;
}

void DialogSettingsThermal::on_buttonBox_accepted()
{

}

void DialogSettingsThermal::on_buttonBox_rejected()
{

}

void DialogSettingsThermal::on_spinBoxADC1_valueChanged(int arg1)
{
	const double volt = 5.0 / 1024.0 * arg1;
	ui->doubleSpinBoxADC1V->setValue(volt);
}

void DialogSettingsThermal::on_doubleSpinBoxADC1V_valueChanged(double arg1)
{
	const int adc = int(1024.0 / 5.0 * arg1);
	ui->spinBoxADC1->setValue(adc);
}

void DialogSettingsThermal::on_spinBoxADC2_valueChanged(int arg1)
{
	const double volt = 5.0 / 1024.0 * arg1;
	ui->doubleSpinBoxADC2V->setValue(volt);
}

void DialogSettingsThermal::on_doubleSpinBoxADC2V_valueChanged(double arg1)
{
	const int adc = int(1024.0 / 5.0 * arg1);
	ui->spinBoxADC2->setValue(adc);
}

void DialogSettingsThermal::setPoint1(const int ADC, const double Grad)
{
	ui->spinBoxADC1->setValue(ADC);
	ui->doubleSpinBoxADC1Grad->setValue(Grad);
}

void DialogSettingsThermal::setPoint2(const int ADC, const double Grad)
{
	ui->spinBoxADC2->setValue(ADC);
	ui->doubleSpinBoxADC2Grad->setValue(Grad);
}

DialogSettingsThermal::PairADCGrad DialogSettingsThermal::getPoint1(void) const
{
	const PairADCGrad retVal(ui->spinBoxADC1->value(),
			ui->doubleSpinBoxADC1Grad->value());
	return retVal;
}

DialogSettingsThermal::PairADCGrad DialogSettingsThermal::getPoint2(void) const
{
	const PairADCGrad retVal(ui->spinBoxADC2->value(),
			ui->doubleSpinBoxADC2Grad->value());
	return retVal;
}

void DialogSettingsThermal::setMin(const double grad)
{
	ui->doubleSpinBoxMin->setValue(grad);
}

void DialogSettingsThermal::setMax(const double grad)
{
	ui->doubleSpinBoxMax->setValue(grad);
}

void DialogSettingsThermal::setNominal(const double grad)
{
	ui->doubleSpinBoxNominal->setValue(grad);
}

void DialogSettingsThermal::setCritical(const double grad)
{
	ui->doubleSpinBoxCritical->setValue(grad);
}

double DialogSettingsThermal::getMin(void) const
{
	return ui->doubleSpinBoxMin->value();
}

double DialogSettingsThermal::getMax(void) const
{
	return ui->doubleSpinBoxMax->value();
}

double DialogSettingsThermal::getNominal(void) const
{
	return ui->doubleSpinBoxNominal->value();
}

double DialogSettingsThermal::getCritical(void) const
{
	return ui->doubleSpinBoxCritical->value();
}

void DialogSettingsThermal::on_doubleSpinBoxMin_valueChanged(double arg1)
{
	ui->thermometer->setMinimum(arg1);
}

void DialogSettingsThermal::on_doubleSpinBoxMax_valueChanged(double arg1)
{
	ui->thermometer->setMaximum(arg1);
}

void DialogSettingsThermal::on_doubleSpinBoxNominal_valueChanged(double arg1)
{
	ui->thermometer->setNominal(arg1);
}

void DialogSettingsThermal::on_doubleSpinBoxCritical_valueChanged(double arg1)
{
	ui->thermometer->setCritical(arg1);
}
