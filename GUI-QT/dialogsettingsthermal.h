#ifndef DIALOGSETTINGSTHERMAL_H
#define DIALOGSETTINGSTHERMAL_H

#include <QDialog>

namespace Ui
{
class DialogSettingsThermal;
}

class DialogSettingsThermal: public QDialog
{
Q_OBJECT

public:
	typedef std::pair<int, double> PairADCGrad;

public:
	explicit DialogSettingsThermal(QWidget *parent = 0);
	~DialogSettingsThermal();

	void setPoint1(const int ADC, const double Grad);
	void setPoint2(const int ADC, const double Grad);

	PairADCGrad getPoint1(void) const;
	PairADCGrad getPoint2(void) const;

	void setMin(const double grad);
	void setMax(const double grad);
	void setNominal(const double grad);
	void setCritical(const double grad);

	double getMin(void) const;
	double getMax(void) const;
	double getNominal(void) const;
	double getCritical(void) const;

private slots:
	void on_buttonBox_accepted();
	void on_buttonBox_rejected();

	void on_spinBoxADC1_valueChanged(int arg1);
	void on_doubleSpinBoxADC1V_valueChanged(double arg1);

	void on_spinBoxADC2_valueChanged(int arg1);
	void on_doubleSpinBoxADC2V_valueChanged(double arg1);

	void on_doubleSpinBoxMin_valueChanged(double arg1);

	void on_doubleSpinBoxMax_valueChanged(double arg1);

	void on_doubleSpinBoxNominal_valueChanged(double arg1);

	void on_doubleSpinBoxCritical_valueChanged(double arg1);

private:
	Ui::DialogSettingsThermal *ui;
};

#endif // DIALOGSETTINGSTHERMAL_H
