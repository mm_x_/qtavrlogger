#include <QtGui/QApplication>
#include "qavrlogger.h"

int main(int argc, char *argv[]) {
	QCoreApplication::setOrganizationName("Sonory");
	QCoreApplication::setOrganizationDomain("sonory.com");
	QCoreApplication::setApplicationName("AVR Logger");

	QApplication a(argc, argv);
	qAVRLogger w;
	w.show();

	return a.exec();
}
