#include "qavrlogger.h"
#include "dialogsettingsthermal.h"

#include <qsettings.h>

const double qAVRLogger::s_ADC1TempHigh = 1000;
const double qAVRLogger::s_ADC1High = 919;
const double qAVRLogger::s_ADC1TempLow = 300.0;
const double qAVRLogger::s_ADC1Low = 0;

const double qAVRLogger::s_TempNominal1 = 200;
const double qAVRLogger::s_TempCritical1 = 600;

const double qAVRLogger::s_ADC2TempHigh = 150.0;
const double qAVRLogger::s_ADC2High = 1060;
const double qAVRLogger::s_ADC2TempLow = -50.0;
const double qAVRLogger::s_ADC2Low = 0;

const double qAVRLogger::s_TempNominal2 = -10;
const double qAVRLogger::s_TempCritical2 = 60;

#ifdef _WIN32
const QString defPathTTY = "COM1";
const QString defPathLogFile = "C:\\record\\out.csv";
#else
const QString defPathTTY = "/dev/ttyUSB0";
const QString defPathLogFile = "/var/out.csv";
#endif

qAVRLogger::qAVRLogger(QWidget *parent) :
		QMainWindow(parent), m_system(this), m_pFileSave(), m_freq()
{
	ui.setupUi(this);

	m_pTimerSave = new QTimer(this);
	connect(m_pTimerSave, SIGNAL(timeout()), this, SLOT(on_saveTick()));

	m_pTimerUpdate = new QTimer(this);
	connect(m_pTimerUpdate, SIGNAL(timeout()), this, SLOT(on_updateUI()));
	m_pTimerUpdate->start(50);

	ui.cbPort->setEditText(defPathTTY);

	ui.thermometer->setMaximum(s_ADC1TempHigh);
	ui.thermometer->setCritical(s_TempCritical1);
	ui.thermometer->setNominal(s_TempNominal1);
	ui.thermometer->setMinimum(s_ADC1Low);

	m_interADCTemp[0].set_begin(s_ADC1Low, s_ADC1TempLow);
	m_interADCTemp[0].set_end(s_ADC1High, s_ADC1TempHigh);

	ui.thermometer_2->setMaximum(s_ADC2TempHigh);
	ui.thermometer_2->setCritical(s_TempCritical2);
	ui.thermometer_2->setNominal(s_TempNominal2);
	ui.thermometer_2->setMinimum(s_ADC2Low);

	m_interADCTemp[1].set_begin(s_ADC2Low, s_ADC2TempLow);
	m_interADCTemp[1].set_end(s_ADC2High, s_ADC2TempHigh);

	memset(m_ADC, 0, sizeof(m_ADC));

	for (size_t pos = 0; pos < 8; ++pos)
		loadThermoMeter(pos);

	QSettings settings;
	ui.dMultFreq->setValue(
			settings.value("Turn/mult", ui.dMultFreq->value()).value<double>());
}

qAVRLogger::~qAVRLogger()
{
	delete m_pTimerSave;
	delete m_pFileSave;
}

void qAVRLogger::on_btConnect_clicked()
{
	if (!isStart())
		start(ui.cbPort->currentText().toLocal8Bit());
	else
		stop();
}

bool qAVRLogger::start(const char *path)
{
	m_pFileSave = new QFile(defPathLogFile);
	if (m_pFileSave->exists())
	{
		if (QMessageBox::question(this, tr("File exist ..."),
				tr("Replace with file C:\\record\\out.csv ?"), QMessageBox::Yes,
				QMessageBox::No) != QMessageBox::Yes)
			return false;

		if (!m_pFileSave->remove())
		{
			QMessageBox::critical(this, tr("Error"),
					"Can`t remove C:\\record\\out.csv");
			return false;
		}
	}

	if (!m_system.connect(path))
	{
		QMessageBox::critical(this, tr("Error"),
				tr(m_system.getLastErrorMsg()));
		return false;
	}

	m_timeStartSave = QTime::currentTime();
	m_system.start();

	if (!m_pFileSave->open(QIODevice::ReadWrite | QIODevice::Text))
	{
		QMessageBox::critical(this, tr("Error"),
				tr("Can`t create C:\\record\\out.csv"));
		delete m_pFileSave;
		m_pFileSave = NULL;
	}
	else
	{
		m_pFileSave->write("Time, Turn, ADC1, ADC2,\n");
		m_pFileSave->flush();
		m_pTimerSave->start(100);
	}

	return true;
}

void qAVRLogger::stop()
{
	m_pTimerSave->stop();
	m_system.disconnect();

	delete m_pFileSave;
	m_pFileSave = NULL;
}

bool qAVRLogger::isStart()
{
	return m_system.isConnect();
}

void qAVRLogger::paintEvent(QPaintEvent * event)
{
	if (isStart())
		ui.btConnect->setText("Disconnect");
	else
		ui.btConnect->setText("Connect");
}

void qAVRLogger::put_freq(const double freq)
{
	QMutexLocker lock(&m_mutexFreq);
	m_freq = freq;
}

void qAVRLogger::put_ADC(const double val, const size_t ch)
{
	assert(ch >= 0 && ch < 8);
	QMutexLocker lock(&m_mutexADC[ch]);
	m_ADC[ch] = val;
}

void qAVRLogger::on_saveTick()
{
	if (!m_pFileSave)
		return;

	const QString strmsg = tr("%1, %2, %3, %4, %5, %6, %7, %8, %9, %10\n").arg(
			m_timeStartSave.msecsTo(QTime::currentTime())).arg(
			getFreq() * 60.0 * ui.dMultFreq->value()).arg(getTemp(0)).arg(
			getTemp(1)).arg(getTemp(2)).arg(getTemp(3)).arg(getTemp(4)).arg(
			getTemp(5)).arg(getTemp(6)).arg(getTemp(7));
	m_pFileSave->write(strmsg.toLocal8Bit());
	m_pFileSave->flush();
}

double qAVRLogger::getTemp(const size_t ch)
{
	return m_interADCTemp[ch](getAdc(ch));
}

void qAVRLogger::on_updateUI()
{
	ui.manometer->setValue(getFreq() * 60.0 * ui.dMultFreq->value());

	ui.thermometer->setValue(getTemp(0));
	ui.thermometer_2->setValue(getTemp(1));
	ui.thermometer_3->setValue(getTemp(2));
	ui.thermometer_4->setValue(getTemp(3));
	ui.thermometer_5->setValue(getTemp(4));
	ui.thermometer_6->setValue(getTemp(5));
	ui.thermometer_7->setValue(getTemp(6));
	ui.thermometer_8->setValue(getTemp(7));
}

ThermoMeter* qAVRLogger::getThermoMeter(const size_t num)
{
	assert(num >= 0 && num < 8);
	ThermoMeter* cur = NULL;
	switch (num)
	{
	case 0:
		cur = ui.thermometer;
		break;
	case 1:
		cur = ui.thermometer_2;
		break;
	case 2:
		cur = ui.thermometer_3;
		break;
	case 3:
		cur = ui.thermometer_4;
		break;
	case 4:
		cur = ui.thermometer_5;
		break;
	case 5:
		cur = ui.thermometer_6;
		break;
	case 6:
		cur = ui.thermometer_7;
		break;
	case 7:
		cur = ui.thermometer_8;
		break;
	default:
		assert("support only 8 thermal controls");
	}

	return cur;
}

void qAVRLogger::saveThermoMeter(const size_t num)
{
	ThermoMeter* cur = getThermoMeter(num);
	if (!cur)
		return;

	QSettings settings;
	settings.setValue(QString("Thermometer%1").arg(num) + "/min",
			cur->minimum());
	settings.setValue(QString("Thermometer%1").arg(num) + "/max",
			cur->maximum());
	settings.setValue(QString("Thermometer%1").arg(num) + "/nominal",
			cur->nominal());
	settings.setValue(QString("Thermometer%1").arg(num) + "/critical",
			cur->critical());

	settings.setValue(QString("Thermometer%1").arg(num) + "/Point1/ADC",
			int(m_interADCTemp[num].get_begin().first));
	settings.setValue(QString("Thermometer%1").arg(num) + "/Point1/Grad",
			m_interADCTemp[num].get_begin().second);

	settings.setValue(QString("Thermometer%1").arg(num) + "/Point2/ADC",
			int(m_interADCTemp[num].get_end().first));
	settings.setValue(QString("Thermometer%1").arg(num) + "/Point2/Grad",
			m_interADCTemp[num].get_end().second);
}

void qAVRLogger::loadThermoMeter(const size_t num)
{
	ThermoMeter* cur = getThermoMeter(num);
	if (!cur)
		return;

	QSettings settings;
	const double min = settings.value(
			QString("Thermometer%1").arg(num) + "/min", cur->minimum()).value<
			double>();
	const double max = settings.value(
			QString("Thermometer%1").arg(num) + "/max", cur->maximum()).value<
			double>();
	const double nominal =
			settings.value(QString("Thermometer%1").arg(num) + "/nominal",
					cur->nominal()).value<double>();
	const double critical =
			settings.value(QString("Thermometer%1").arg(num) + "/critical",
					cur->critical()).value<double>();

	const int adc1 = settings.value(
			QString("Thermometer%1").arg(num) + "/Point1/ADC",
			m_interADCTemp[num].get_begin().first).value<int>();
	const double grad1 = settings.value(
			QString("Thermometer%1").arg(num) + "/Point1/Grad",
			m_interADCTemp[num].get_begin().second).value<double>();
	const int adc2 = settings.value(
			QString("Thermometer%1").arg(num) + "/Point2/ADC",
			m_interADCTemp[num].get_end().first).value<int>();
	const double grad2 = settings.value(
			QString("Thermometer%1").arg(num) + "/Point2/Grad",
			m_interADCTemp[num].get_end().second).value<double>();

	cur->setMinimum(min);
	cur->setMaximum(max);
	cur->setNominal(nominal);
	cur->setCritical(critical);

	m_interADCTemp[num].set_begin(adc1, grad1);
	m_interADCTemp[num].set_end(adc2, grad2);
}

void qAVRLogger::execThermalSettings(const size_t num)
{
	ThermoMeter* cur = getThermoMeter(num);
	if (!cur)
		return;

	DialogSettingsThermal * dialog = new DialogSettingsThermal(this);
	dialog->setMin(cur->minimum());
	dialog->setMax(cur->maximum());
	dialog->setNominal(cur->nominal());
	dialog->setCritical(cur->critical());

	dialog->setPoint1(m_interADCTemp[num].get_begin().first,
			m_interADCTemp[num].get_begin().second);
	dialog->setPoint2(m_interADCTemp[num].get_end().first,
			m_interADCTemp[num].get_end().second);

	dialog->exec();

	if (dialog->result() == QDialog::Accepted)
	{
		cur->setMinimum(dialog->getMin());
		cur->setMaximum(dialog->getMax());
		cur->setNominal(dialog->getNominal());
		cur->setCritical(dialog->getCritical());

		const DialogSettingsThermal::PairADCGrad pairPoint1 =
				dialog->getPoint1();
		m_interADCTemp[num].set_begin(pairPoint1.first, pairPoint1.second);
		const DialogSettingsThermal::PairADCGrad pairPoint2 =
				dialog->getPoint2();
		m_interADCTemp[num].set_end(pairPoint2.first, pairPoint2.second);
	}
}
void qAVRLogger::on_actionT1_activated()
{
	execThermalSettings(0);
	saveThermoMeter(0);
}

void qAVRLogger::on_actionT2_activated()
{
	execThermalSettings(1);
	saveThermoMeter(1);
}

void qAVRLogger::on_actionT3_activated()
{
	execThermalSettings(2);
	saveThermoMeter(2);
}

void qAVRLogger::on_actionT4_activated()
{
	execThermalSettings(3);
	saveThermoMeter(3);
}

void qAVRLogger::on_actionT5_activated()
{
	execThermalSettings(4);
	saveThermoMeter(4);
}

void qAVRLogger::on_actionT6_activated()
{
	execThermalSettings(5);
	saveThermoMeter(5);
}

void qAVRLogger::on_actionT7_activated()
{
	execThermalSettings(6);
	saveThermoMeter(6);
}

void qAVRLogger::on_actionT8_activated()
{
	execThermalSettings(7);
	saveThermoMeter(7);
}

void qAVRLogger::on_thermometer_customContextMenuRequested(const QPoint &pos)
{
	QList<QAction*> action;
	action.push_back(ui.actionT1);

	QMenu contextMenu(tr("Context menu"), this);
	contextMenu.addActions(action);
	contextMenu.exec(ui.thermometer->mapToGlobal(pos));
}

void qAVRLogger::on_thermometer_2_customContextMenuRequested(const QPoint &pos)
{
	QList<QAction*> action;
	action.push_back(ui.actionT2);

	QMenu contextMenu(tr("Context menu"), this);
	contextMenu.addActions(action);
	contextMenu.exec(ui.thermometer->mapToGlobal(pos));
}

void qAVRLogger::on_thermometer_3_customContextMenuRequested(const QPoint &pos)
{
	QList<QAction*> action;
	action.push_back(ui.actionT3);

	QMenu contextMenu(tr("Context menu"), this);
	contextMenu.addActions(action);
	contextMenu.exec(ui.thermometer->mapToGlobal(pos));
}

void qAVRLogger::on_thermometer_4_customContextMenuRequested(const QPoint &pos)
{
	QList<QAction*> action;
	action.push_back(ui.actionT4);

	QMenu contextMenu(tr("Context menu"), this);
	contextMenu.addActions(action);
	contextMenu.exec(ui.thermometer->mapToGlobal(pos));
}

void qAVRLogger::on_thermometer_5_customContextMenuRequested(const QPoint &pos)
{
	QList<QAction*> action;
	action.push_back(ui.actionT5);

	QMenu contextMenu(tr("Context menu"), this);
	contextMenu.addActions(action);
	contextMenu.exec(ui.thermometer->mapToGlobal(pos));
}

void qAVRLogger::on_thermometer_6_customContextMenuRequested(const QPoint &pos)
{
	QList<QAction*> action;
	action.push_back(ui.actionT6);

	QMenu contextMenu(tr("Context menu"), this);
	contextMenu.addActions(action);
	contextMenu.exec(ui.thermometer->mapToGlobal(pos));
}

void qAVRLogger::on_thermometer_7_customContextMenuRequested(const QPoint &pos)
{
	QList<QAction*> action;
	action.push_back(ui.actionT7);

	QMenu contextMenu(tr("Context menu"), this);
	contextMenu.addActions(action);
	contextMenu.exec(ui.thermometer->mapToGlobal(pos));
}

void qAVRLogger::on_thermometer_8_customContextMenuRequested(const QPoint &pos)
{
	QList<QAction*> action;
	action.push_back(ui.actionT8);

	QMenu contextMenu(tr("Context menu"), this);
	contextMenu.addActions(action);
	contextMenu.exec(ui.thermometer->mapToGlobal(pos));
}

void qAVRLogger::on_dMultFreq_valueChanged(double arg1)
{
	QSettings settings;
	settings.setValue("Turn/mult", arg1);
}
