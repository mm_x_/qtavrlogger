#ifndef QAVRLOGGER_H
#define QAVRLOGGER_H

#include <QtGui/QMainWindow>
#include <QtGui/QMessageBox>
#include <QtCore/QMutex>
#include <QtCore/QTimer>
#include <QtCore/QFile>
#include <QtCore/QTime>

#include <enginesound/algorithm/Interpolation.h>

#include <ComPortSystem.h>

#include "ui_qavrlogger.h"

#include <cassert>

class qAVRLogger: public QMainWindow
{
Q_OBJECT
	const static double s_ADC1TempHigh;
	const static double s_ADC1High;
	const static double s_ADC1TempLow;
	const static double s_ADC1Low;

	const static double s_TempNominal1;
	const static double s_TempCritical1;

	const static double s_ADC2TempHigh;
	const static double s_ADC2High;
	const static double s_ADC2TempLow;
	const static double s_ADC2Low;

	const static double s_TempNominal2;
	const static double s_TempCritical2;

	typedef enginesound::LinearInterpolation<double, double, double> ADCTempInterpolation;
public:
	qAVRLogger(QWidget *parent = 0);
	~qAVRLogger();

	bool start(const char * path);
	void stop();
	bool isStart();

	double getAdc(const size_t ch)
	{
		QMutexLocker lock(&m_mutexADC[ch]);
		return m_ADC[ch];
	}

	double getFreq()
	{
		QMutexLocker lock(&m_mutexFreq);
		return m_freq;
	}

	double getTemp(const size_t ch);

	void put_freq(const double freq);
	void put_ADC(const double val, const size_t ch);
protected:
	ThermoMeter* getThermoMeter(const size_t num);

	void saveThermoMeter(const size_t num);
	void loadThermoMeter(const size_t num);

	void paintEvent(QPaintEvent *event);
	void execThermalSettings(const size_t num);

private slots:
	void on_btConnect_clicked();
	void on_saveTick();
	void on_updateUI();

	void on_actionT1_activated();
	void on_actionT2_activated();
	void on_actionT3_activated();
	void on_actionT4_activated();
	void on_actionT5_activated();
	void on_actionT6_activated();
	void on_actionT7_activated();
	void on_actionT8_activated();

	void on_thermometer_customContextMenuRequested(const QPoint &pos);
	void on_thermometer_2_customContextMenuRequested(const QPoint &pos);
	void on_thermometer_3_customContextMenuRequested(const QPoint &pos);
	void on_thermometer_4_customContextMenuRequested(const QPoint &pos);
	void on_thermometer_5_customContextMenuRequested(const QPoint &pos);
	void on_thermometer_6_customContextMenuRequested(const QPoint &pos);
	void on_thermometer_7_customContextMenuRequested(const QPoint &pos);
	void on_thermometer_8_customContextMenuRequested(const QPoint &pos);

	void on_dMultFreq_valueChanged(double arg1);

private:
	Ui::qAVRLoggerClass ui;
	ComPortSystem m_system;
	QTimer * m_pTimerSave;
	QTimer * m_pTimerUpdate;
	QFile * m_pFileSave;
	QTime m_timeStartSave;

	QMutex m_mutexFreq;
	double m_freq;

	QMutex m_mutexADC[8];
	ADCTempInterpolation m_interADCTemp[8];
	double m_ADC[8];
};

#endif // QAVRLOGGER_H
