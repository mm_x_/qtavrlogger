#-------------------------------------------------
#
# Project created by QtCreator 2013-05-16T13:25:00
#
#-------------------------------------------------

QT       -= core gui

TARGET = esLibrary
TEMPLATE = lib
CONFIG += staticlib
INCLUDEPATH += ./include/

SOURCES += \
    src/TimeLatency.cpp \
    src/Time.cpp \
    src/ThreadCallBack.cpp \
    src/Thread.cpp \
    src/RationalNumber.cpp \
    src/PThread.cpp \
    src/Object.cpp \
    src/LogTTY.cpp \
    src/LogStd.cpp \
    src/LogFile.cpp \
    src/LogBase.cpp \
    src/Log.cpp \
    src/ErrorObject.cpp \
    src/CryptSahara.cpp \
    src/CryptLinux.cpp \
    src/CryptGCrypt.cpp \
    src/CryptBase.cpp \
    src/BoostThread.cpp

HEADERS += \
    include/enginesound/algorithm/comparator/RegionPolicy.h \
    include/enginesound/algorithm/comparator/CalcPolicy.h \
    include/enginesound/algorithm/InterpolationHighLow.h \
    include/enginesound/algorithm/Interpolation.h \
    include/enginesound/algorithm/Inertia.h \
    include/enginesound/algorithm/Comparator.h \
    include/enginesound/algorithm/Check.h \
    include/enginesound/algorithm/AvgBuff.h \
    include/enginesound/algorithm/Algorithm.h \
    include/enginesound/core/Object.h \
    include/enginesound/core/ErrorObject.h \
    include/enginesound/core/CallBackBase.h \
    include/enginesound/crypt/CryptSahara.h \
    include/enginesound/crypt/CryptLinux.h \
    include/enginesound/crypt/CryptGCrypt.h \
    include/enginesound/crypt/CryptBase.h \
    include/enginesound/log/LogTTY.h \
    include/enginesound/log/LogStd.h \
    include/enginesound/log/LogFile.h \
    include/enginesound/log/LogBase.h \
    include/enginesound/log/Log.h \
    include/enginesound/statistic/TimeLatency.h \
    include/enginesound/system/Time.h \
    include/enginesound/system/ThreadCallBack.h \
    include/enginesound/system/Thread.h \
    include/enginesound/system/PThread.h \
    include/enginesound/system/BoostThread.h \
    include/enginesound/RationalNumber.h \
    include/enginesound/Macros.h \
    include/enginesound/Log.h \
    include/enginesound/BaseObject.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
