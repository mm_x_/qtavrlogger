/*
 * BaseObject.h
 *
 *  Created on: 16.07.2010
 *      Author: user2
 */

#ifndef BASEOBJECT_H_
#define BASEOBJECT_H_

#include <enginesound/core/Object.h>
#include <enginesound/core/ErrorObject.h>

namespace enginesound
{

typedef Object BaseObject;

}

#endif /* BASEOBJECT_H_ */
