/*
 * Macros.h
 *
 *  Created on: 04.08.2010
 *      Author: user2
 */

#ifndef MACROS_H_
#define MACROS_H_

#ifdef ES_UNUSED
#elif defined(__GNUC__)
# define ES_UNUSED(x) UNUSED_ ## x __attribute__((unused))
#elif defined(__LCLINT__)
# define ES_UNUSED(x) /*@unused@*/ x
#else
# define ES_UNUSED(x) x
#endif

#endif /* MACROS_H_ */
