/*
 * RationalNumber.h
 *
 *  Created on: 23.04.2012
 *      Author: u4
 */

#ifndef RATIONALNUMBER_H_
#define RATIONALNUMBER_H_

#include <boost/rational.hpp>

namespace enginesound
{

class RationalNumber
{
public:
	RationalNumber() :
			data()
	{

	}
	virtual ~RationalNumber()
	{

	}

	RationalNumber(const boost::rational<int> & eq) :
			data(eq)
	{

	}

	RationalNumber(float n) :
			data(fromDouble(n))
	{

	}

	RationalNumber(double n) :
			data(fromDouble(n))
	{

	}

	RationalNumber(int n) :
			data(n)
	{
	}
	RationalNumber(unsigned int n) :
			data(n)
	{
	}

	RationalNumber(int n, int d) :
			data(n, d)
	{
	}

	// Arithmetic assignment operators
	RationalNumber& operator+=(const RationalNumber& r)
	{
		data += r.data;
		return *this;
	}
	RationalNumber& operator-=(const RationalNumber& r)
	{
		data -= r.data;
		return *this;
	}
	RationalNumber& operator*=(const RationalNumber& r)
	{
		data *= r.data;
		return *this;
	}
	RationalNumber& operator/=(const RationalNumber& r)
	{
		data /= r.data;
		return *this;
	}

	RationalNumber operator+(const RationalNumber& r) const
	{
		RationalNumber retVal(*this);
		retVal.data += r.data;
		return retVal;
	}
	RationalNumber operator-(const RationalNumber& r) const
	{
		RationalNumber retVal(*this);
		retVal.data -= r.data;
		return retVal;
	}
	RationalNumber operator*(const RationalNumber& r) const
	{
		RationalNumber retVal(*this);
		retVal.data *= r.data;
		return retVal;
	}
	RationalNumber operator/(const RationalNumber& r) const
	{
		RationalNumber retVal(*this);
		retVal.data /= r.data;
		return retVal;
	}

	bool operator!=(const RationalNumber& r) const
	{
		return data != r.data;
	}

	// Boolean conversion

	// Comparison operators
	bool operator<(const RationalNumber& r) const
	{
		return this->data < r.data;
	}
	bool operator>(const RationalNumber& r) const
	{
		return this->data > r.data;
	}
	bool operator<=(const RationalNumber& r) const
	{
		return this->data <= r.data;
	}
	bool operator>=(const RationalNumber& r) const
	{
		return this->data >= r.data;
	}
	bool operator==(const RationalNumber& r) const
	{
		return this->data == r.data;
	}

	boost::rational<int> & toBase(void)
	{
		return data;
	}
	int toInt(void) const
	{
		return data.numerator() / data.denominator();
	}
	double toFloat(void) const
	{
		return float(data.numerator()) / float(data.denominator());
	}
	double toDouble(void) const
	{
		return double(data.numerator()) / double(data.denominator());
	}

	static boost::rational<int> fromDouble(const double val)
	{
		const boost::rational<int> retVal(int(val * 100000), 100000);

		return retVal;
	}
private:
	boost::rational<int> data;
};

inline std::istream& operator>>(std::istream& is, RationalNumber& r)
{
	is >> r.toBase();

	return is;
}

inline std::ostream& operator<<(std::ostream& os, RationalNumber r)
{
	os << r.toBase();

	return os;
} /* namespace enginesound */

}
#endif /* RATIONALNUMBER_H_ */
