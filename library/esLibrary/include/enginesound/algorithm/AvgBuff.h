/*
 * AvgBuff.h
 *
 *  Created on: 16.03.2010
 *      Author: user
 */

#ifndef ESAVGBUFF_H_
#define ESAVGBUFF_H_

#include <boost/circular_buffer.hpp>

namespace enginesound
{
template<typename _Iterator, typename _Calc>
class AvgBuff
{
	typedef typename std::iterator_traits<_Iterator>::difference_type
			difference_type;
	typedef typename std::iterator_traits<_Iterator>::value_type value_type;

	typedef boost::circular_buffer<value_type> Buff;

	static const difference_type min_size = 1;
public:
	AvgBuff(const difference_type capacity = min_size) :
		m_buff(capacity), m_Summ()
	{
	}

	~AvgBuff()
	{
	}

	void push(const value_type val)
	{
		m_Summ += val;

		if (full())
		{
			m_Summ -= *m_buff.begin();
		}
		m_buff.push_back(val);

	}

	value_type get() const
	{
		if (m_buff.size() == 0)
			return 0;

		return m_Summ / _Calc(m_buff.size());
	}

	value_type operator()(const value_type val)
	{
		push(val);
		return get();
	}

	void set_new_capacity(const difference_type capacity)
	{
		m_Summ = 0;

		m_buff.clear();
		m_buff.set_capacity(capacity);
	}

	difference_type get_capacity(void) const
	{
		return m_buff.capacity();
	}

	bool full() const
	{
		return m_buff.full();
	}

	void clear()
	{
		m_Summ = 0;
		m_buff.clear();
	}
private:
	Buff m_buff;

	_Calc m_Summ;
};

}

#endif /* ESAVGBUFF_H_ */
