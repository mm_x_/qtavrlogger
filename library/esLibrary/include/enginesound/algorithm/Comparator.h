/*
 * Comparator.h
 *
 *  Created on: 16.03.2010
 *      Author: user
 */

#ifndef ESCOMPARATOR_H_
#define ESCOMPARATOR_H_

#include "enginesound/algorithm/comparator/CalcPolicy.h"
#include <boost/tr1/memory.hpp>

namespace enginesound
{

template<typename _Iterator, typename _Calc>
class Comparator
{
public:
	typedef typename std::iterator_traits<_Iterator>::difference_type
			difference_type;
	typedef typename std::iterator_traits<_Iterator>::value_type value_type;

	typedef typename comparator::CalcPolicy<_Iterator, _Calc> calc_policy;
	typedef typename comparator::CalcPolicy<_Iterator, _Calc>::Ptr
			calc_policy_ptr;
	typedef typename comparator::CalcPolicy<_Iterator, _Calc>::value_type
			calc_policy_value_type;

	typedef std::tr1::shared_ptr<Comparator> Ptr;
	static const size_t defSizeAvgBuff = 4096 * 5;

	class CallBack
	{
	public:
		typedef std::tr1::shared_ptr<CallBack> Ptr;
	public:
		CallBack()
		{

		}

		virtual void setInterval(const calc_policy_value_type val) = 0;
	};
public:
	Comparator(calc_policy_ptr policy = calc_policy_ptr()) :
		m_pPolicyCalcAvg(policy), m_framesInterval(), m_framesIntervalPrev(),
				m_multiply(1)
	{
		clear();
	}

	~Comparator()
	{

	}

	void clear()
	{
		m_prevMoreThanAvg = false;
		m_numPeriod = 0;
		m_numFramesIntervalSumm = 0;
		m_framesIntervalSumm = 0;
		m_framesIntervalCalc = 0;
	}

	calc_policy_value_type getLowRegion() const
	{
		return m_pPolicyCalcAvg->getLow() * getMultAvg();
	}

	calc_policy_value_type getHighRegion() const
	{
		return m_pPolicyCalcAvg->getHigh() * getMultAvg();
	}

	void calc(const _Iterator begin, const _Iterator end)
	{
		static size_t sPos = 0;
		m_pPolicyCalcAvg->calc(begin, end);

		const calc_policy_value_type low = getLowRegion();
		const calc_policy_value_type high = getHighRegion();

		for (_Iterator pos = begin; pos < end; ++pos)
		{
			calc_policy_value_type val = *pos;
			if (m_prevMoreThanAvg)
			{
				if (val < low)
				{
					m_prevMoreThanAvg = false;
					m_numPeriod++;

					if (m_pCallBack)
						m_pCallBack->setInterval(m_framesIntervalCalc);

					if (m_numPeriod >= 1)
					{
						m_framesIntervalSumm += m_framesIntervalCalc;
						m_numFramesIntervalSumm++;
						m_framesIntervalCalc = 1;
						m_numPeriod = 0;
					}
				}
				else
					m_framesIntervalCalc++;
			}
			else
			{
				if (val > high)
				{
					m_prevMoreThanAvg = true;
				}
				m_framesIntervalCalc++;
			}
		}

		if (m_numFramesIntervalSumm > 0)
		{
			setFramesInterval(m_framesIntervalSumm / m_numFramesIntervalSumm);
			m_framesIntervalSumm = 0;
			m_numFramesIntervalSumm = 0;
		}

		sPos += distance(begin, end);
	}

	difference_type getInterval()
	{
		return m_framesInterval;
	}

	difference_type getIntervalPrev()
	{
		return m_framesIntervalPrev;
	}

	typename calc_policy::Ptr getPolicyCalcAvg() const
	{
		return m_pPolicyCalcAvg;
	}

	void setPolicyCalcAvg(typename calc_policy::Ptr m_pPolicyCalcAvg)
	{
		this->m_pPolicyCalcAvg = m_pPolicyCalcAvg;
	}

	void setCallBack(typename CallBack::Ptr pObj)
	{
		m_pCallBack = pObj;
	}

	typename CallBack::Ptr getCallBack(void)
	{
		return m_pCallBack;
	}

	virtual void printStat(std::ostream & out)
	{
		m_pPolicyCalcAvg->printStat(out);
	}
protected:
	void setFramesInterval(const difference_type framesInterval)
	{
		m_framesIntervalPrev = m_framesInterval;
		m_framesInterval = framesInterval;
	}

	void setMultAvg(const float val)
	{
		m_multiply = val;
	}

	float getMultAvg(void) const
	{
		return m_multiply;
	}
private:
	typename calc_policy::Ptr m_pPolicyCalcAvg;

	difference_type m_framesInterval;
	difference_type m_framesIntervalPrev;

	float m_multiply;

	typename CallBack::Ptr m_pCallBack;

	/* need for calc */
	bool m_prevMoreThanAvg;
	size_t m_numPeriod;
	size_t m_numFramesIntervalSumm;
	size_t m_framesIntervalSumm;
	size_t m_framesIntervalCalc;
};

}

#endif /* ESCOMPARATOR_H_ */
