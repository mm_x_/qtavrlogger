/*
 * Interpolation.h
 *
 *  Created on: 16.10.2009
 *      Author: u4
 */

#ifndef ALGORITHM_INTERPOLATION_H_
#define ALGORITHM_INTERPOLATION_H_

#include <utility>
#include <map>
#include <iostream>

namespace enginesound
{
template<class T_VAL_FROM, class T_VAL_TO, class T_CALC = T_VAL_TO>
class LinearInterpolation
{
public:
	typedef T_VAL_FROM from_type;
	typedef T_VAL_TO to_type;
	typedef T_CALC calc_type;
public:
	LinearInterpolation() :
			m_begin(), m_beginval(), m_end(), m_endval(), m_clip()
	{
	}

	LinearInterpolation(const T_VAL_FROM begin, const T_VAL_TO beginval, const T_VAL_FROM end, const T_VAL_TO endval,
			const bool clip = false) :
			m_begin(begin), m_beginval(beginval), m_end(end), m_endval(endval), m_clip(clip)
	{
	}

	T_VAL_TO operator()(const T_VAL_FROM val) const
	{
		if (m_endval == m_beginval)
			return val;

		const T_VAL_TO stepNot = (m_endval - m_beginval);

		const T_CALC step = T_CALC(stepNot) / T_CALC(m_end - m_begin);
		const T_VAL_FROM temp = T_VAL_FROM(val) - T_VAL_FROM(m_begin);
		const T_VAL_TO retVal = (T_VAL_TO) (temp * step + T_CALC(m_beginval));

		if (m_clip)
		{
			const T_VAL_TO high = std::max(m_beginval, m_endval);
			const T_VAL_TO low = std::min(m_beginval, m_endval);
			if (retVal < low)
				return low;
			if (retVal > high)
				return high;
		}

		return retVal;
	}

	void set_begin(const T_VAL_FROM begin, const T_VAL_TO beginval)
	{
		m_begin = begin;
		m_beginval = beginval;
	}

	void set_end(const T_VAL_FROM end, const T_VAL_TO endval)
	{
		m_end = end;
		m_endval = endval;
	}

	void set_vol(const T_VAL_TO val)
	{
		set_begin(0, val);
		set_end(0, val);
	}

	std::pair<T_VAL_FROM, T_VAL_TO> get_begin(void) const
	{
		return std::make_pair(m_begin, m_beginval);
	}

	std::pair<T_VAL_FROM, T_VAL_TO> get_end(void) const
	{
		return std::make_pair(m_end, m_endval);
	}

	void set_clip(const bool val)
	{
		m_clip = val;
	}

private:
	T_VAL_FROM m_begin;
	T_VAL_TO m_beginval;
	T_VAL_FROM m_end;
	T_VAL_TO m_endval;
	bool m_clip;
};

template<class T_VAL_FROM, class T_VAL_TO, class T_CALC>
class LinearInterpolationPoints
{
public:
	typedef std::map<T_VAL_FROM, T_VAL_TO> ArrayPoints;
	typedef typename ArrayPoints::iterator ArrayPointsIterator;
	typedef typename ArrayPoints::const_iterator ArrayPointsConstIterator;

	typedef T_VAL_FROM from_type;
	typedef T_VAL_TO to_type;
	typedef T_CALC calc_type;

	enum TYPE_BOUNDS
	{
		NONE_BOUNDS = 0, IN_LEFT_BOUNDS = 1, IN_RIGHT_BOUNDS = 2
	};
public:
	LinearInterpolationPoints(const int bound = NONE_BOUNDS) :
			m_points(), m_bounds(bound)
	{
	}

	LinearInterpolationPoints(const T_VAL_FROM begin, const T_VAL_TO beginval)
	{
		insert(begin, beginval);
	}

	LinearInterpolationPoints(const T_VAL_FROM begin, const T_VAL_TO beginval, const T_VAL_FROM end,
			const T_VAL_TO endval, const bool clip = false)
	{
		insert(begin, beginval);
		insert(end, endval);
		m_bounds = clip ? (int) IN_LEFT_BOUNDS + (int) IN_RIGHT_BOUNDS : true;
	}

	void setBoundsType(const int val)
	{
		m_bounds = val;
	}

	bool isLeftBound(void) const
	{
		return ((m_bounds & 0x1) == IN_LEFT_BOUNDS);
	}

	bool isRightBound(void) const
	{
		return ((m_bounds & 0x2) == IN_RIGHT_BOUNDS);
	}

	void clear()
	{
		m_points.clear();
	}

	void insert(const std::pair<T_VAL_FROM, T_VAL_TO> val)
	{
		m_points.insert(val);
	}
	void insert(const T_VAL_FROM region, const T_VAL_TO val)
	{
		m_points[region] = val;
	}
	template<typename _InputIterator>
	void insert_banch(_InputIterator __first, _InputIterator __last)
	{
		m_points.insert(__first, __last);
	}

	T_VAL_TO operator()(const T_VAL_FROM val) const
	{
		T_VAL_FROM begin, end;
		T_VAL_TO beginval, endval;

		if (m_points.empty())
			return val;

		if (isLeftBound())
		{
			if (val <= m_points.begin()->first)
				return m_points.begin()->second;
		}

		if (isRightBound())
		{
			if (val >= (--m_points.end())->first)
				return (--m_points.end())->second;
		}

		if (m_points.size() == 1)
			return m_points.begin()->second;

		std::pair<ArrayPointsConstIterator, ArrayPointsConstIterator> posLowerUpper = m_points.equal_range(val);

		if (posLowerUpper.second == m_points.end())
		{
			posLowerUpper.first--;
			posLowerUpper.second--;
		}

		if (posLowerUpper.first == posLowerUpper.second)
		{
			if (posLowerUpper.first == m_points.begin())
				posLowerUpper.second++;
			else
				posLowerUpper.first--;

			begin = posLowerUpper.first->first;
			beginval = posLowerUpper.first->second;

		}
		else
		{
			begin = posLowerUpper.first->first;
			beginval = posLowerUpper.first->second;
		}

		end = posLowerUpper.second->first;
		endval = posLowerUpper.second->second;

		const T_VAL_TO stepNot = (endval - beginval);
		if (stepNot == T_VAL_TO(0))
			return beginval;

		const T_CALC step = T_CALC(stepNot) / T_CALC(end - begin);
		const T_CALC temp = T_CALC(val - begin);
		const T_VAL_TO retVal = (T_VAL_TO) (T_CALC(temp) * T_CALC(step) + T_CALC(beginval));

		return retVal;
	}
	ArrayPoints & getArray()
	{
		return m_points;
	}
	const ArrayPoints & getArray() const
	{
		return m_points;
	}

	bool empty() const
	{
		return getArray().empty();
	}

	LinearInterpolationPoints<T_VAL_FROM, T_VAL_TO, T_CALC> & operator =(
			const LinearInterpolation<T_VAL_FROM, T_VAL_TO> & interpolation)
	{
		clear();
		insert(interpolation.get_begin());
		insert(interpolation.get_end());

		return *this;
	}
protected:
private:
	ArrayPoints m_points;
	int m_bounds;
};

template<class T_VAL_FROM, class T_VAL_TO, class T_CALC>
std::ostream &operator<<(std::ostream &stream, const LinearInterpolation<T_VAL_FROM, T_VAL_TO, T_CALC> obj)
{
	const std::pair<T_VAL_FROM, T_VAL_TO> begin = obj.get_begin();
	const std::pair<T_VAL_FROM, T_VAL_TO> end = obj.get_end();
	stream << "{ ( " << begin.first << ", " << begin.second << " ), ( " << end.first << ", " << end.second << " ) }";

	return stream;
}

template<class T_VAL_FROM, class T_VAL_TO, class T_CALC>
std::ostream & operator<<(std::ostream &stream, const LinearInterpolationPoints<T_VAL_FROM, T_VAL_TO, T_CALC> obj)
{
	const typename LinearInterpolationPoints<T_VAL_FROM, T_VAL_TO, T_CALC>::ArrayPoints & points = obj.getArray();

	stream << "{ ";
	if (points.empty())
		stream << "NULL";
	else
	{
		for (typename LinearInterpolationPoints<T_VAL_FROM, T_VAL_TO, T_VAL_TO>::ArrayPoints::const_iterator pos =
				points.begin(); pos != points.end(); ++pos)
		{
			if (pos != points.begin())
				stream << ", ";

			stream << (obj.isLeftBound() ? '[' : '(') << " ";
			stream << pos->first << ", " << pos->second << " ";
			stream << (obj.isRightBound() ? ']' : ')');
		}
	}

	stream << " }";

	return stream;
}
}

#endif /* ENGINESOUNDALGORITHM_H_ */
