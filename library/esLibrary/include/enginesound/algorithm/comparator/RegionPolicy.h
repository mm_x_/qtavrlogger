/*
 * RegionPolicy.h
 *
 *  Created on: 17.03.2010
 *      Author: user
 */

#ifndef ESCOMPARATORREGION_H_
#define ESCOMPARATORREGION_H_

#include <boost/tr1/memory.hpp>
#include <enginesound/Macros.h>

namespace enginesound
{
namespace comparator
{

template<typename _Calc>
class RegionPolicy
{
public:
	typedef _Calc value_type;

	typedef std::tr1::shared_ptr<RegionPolicy> Ptr;
public:
	RegionPolicy()
	{
	}

	virtual ~RegionPolicy()
	{
	}

	virtual value_type getLow(const value_type avg) const = 0;
	virtual value_type getHigh(const value_type avg) const = 0;
	virtual const char * name(void) const
	{
		return "unknown";
	}
};

template<typename _Calc>
class RegionPolicyFixed: public RegionPolicy<_Calc>
{
public:
	typedef std::tr1::shared_ptr<RegionPolicyFixed> Ptr;

public:
	RegionPolicyFixed(const typename RegionPolicy<_Calc>::value_type low = 0,
			const typename RegionPolicy<_Calc>::value_type high = 0xff) :
		m_low(low), m_high(high)
	{
	}

	typename RegionPolicy<_Calc>::value_type getLow(
			const typename RegionPolicy<_Calc>::value_type ES_UNUSED(avg)) const
	{
		return getLow();
	}
	typename RegionPolicy<_Calc>::value_type getHigh(
			const typename RegionPolicy<_Calc>::value_type ES_UNUSED(avg)) const
	{
		return getHigh();
	}
	virtual const char * name(void) const
	{
		return "fixed";
	}

public:
	void setHigh(typename RegionPolicy<_Calc>::value_type m_high)
	{
		this->m_high = m_high;
	}

	void setLow(typename RegionPolicy<_Calc>::value_type m_low)
	{
		this->m_low = m_low;
	}

	int getHigh() const
	{
		return m_high;
	}

	int getLow() const
	{
		return m_low;
	}

private:
	typename RegionPolicy<_Calc>::value_type m_low;
	typename RegionPolicy<_Calc>::value_type m_high;

};

template<typename _Calc>
class RegionPolicyMultAvg: public RegionPolicy<_Calc>
{
public:
	typedef std::tr1::shared_ptr<RegionPolicyMultAvg> Ptr;
public:
	RegionPolicyMultAvg() :
		m_multLow(0.9f), m_multHigh(1.1f)
	{

	}
	RegionPolicyMultAvg(const float multLow, const float multHigh) :
		m_multLow(multLow), m_multHigh(multHigh)
	{
	}
public:
	typename RegionPolicy<_Calc>::value_type getLow(
			const typename RegionPolicy<_Calc>::value_type avg) const
	{
		return (typename RegionPolicy<_Calc>::value_type) (avg * getMultLow());
	}
	typename RegionPolicy<_Calc>::value_type getHigh(
			const typename RegionPolicy<_Calc>::value_type avg) const
	{
		return (typename RegionPolicy<_Calc>::value_type) (avg * getMultHigh());
	}
	virtual const char * name(void) const
	{
		return "multiply";
	}

public:
	float getMultHigh() const
	{
		return m_multHigh;
	}

	float getMultLow() const
	{
		return m_multLow;
	}

	void setMultHigh(const float m_multHigh)
	{
		this->m_multHigh = m_multHigh;
	}

	void setMultLow(const float m_multLow)
	{
		this->m_multLow = m_multLow;
	}
private:
	float m_multLow;
	float m_multHigh;
};

}
}

#endif /* ESCOMPARATORREGION_H_ */
