/*
 * CallBackBase.h
 *
 *  Created on: 06.11.2012
 *      Author: u4
 */

#ifndef CALLBACKBASE_H_
#define CALLBACKBASE_H_

#include <enginesound/core/ErrorObject.h>

namespace enginesound
{

class CallBackBase: public ErrorObject
{
public:
	typedef std::tr1::shared_ptr<CallBackBase> Ptr;
public:
	CallBackBase()
	{

	}
	virtual ~CallBackBase()
	{

	}

	virtual void operator()() = 0;
};

} /* namespace enginesound */
#endif /* CALLBACKBASE_H_ */
