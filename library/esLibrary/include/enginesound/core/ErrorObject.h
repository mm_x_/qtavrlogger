/*
 * ErrorObject.h
 *
 *  Created on: 21.11.2011
 *      Author: user
 */

#ifndef ERROROBJECT_H_
#define ERROROBJECT_H_

#include <enginesound/core/Object.h>
#include <string>

#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>

namespace enginesound
{

/*
 * Thread safe
 */
class ErrorObject: public Object
{
public:
	typedef std::tr1::shared_ptr<ErrorObject> Ptr;

public:
	ErrorObject() :
			m_stringError("OK")
	{
	}
	/*TODO: fix copy m_stringError */
	ErrorObject(const ErrorObject & ES_UNUSED(EScopy)) :
			m_stringError("OK")
	{

	}
	virtual ~ErrorObject()
	{
	}

	virtual std::string getLastErrorMsg()
	{
		boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(m_mutexErrorMsg);
		return m_stringError;
	}

protected:
	virtual void setErrorMsg(const std::string & format, ...);
	virtual void setErrorMsg(const char * format, ...);
	virtual void setErrorMsg(const char * format, va_list __ap);

private:
	boost::interprocess::interprocess_mutex m_mutexErrorMsg;
	std::string m_stringError;
};

} /* namespace enginesound */
#endif /* ERROROBJECT_H_ */
