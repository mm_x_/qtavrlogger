/*
 * Object.h
 *
 *  Created on: 21.11.2011
 *      Author: user
 */

#ifndef OBJECT_H_
#define OBJECT_H_

#include <boost/tr1/memory.hpp>
#include <enginesound/Macros.h>

namespace enginesound
{

class Object
{
public:
	typedef std::tr1::shared_ptr<Object> Ptr;
public:
	Object()
	{
	}
	virtual ~Object()
	{
	}
};

} /* namespace enginesound */
#endif /* OBJECT_H_ */
