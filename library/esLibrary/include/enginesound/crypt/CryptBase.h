/*
 * CryptBase.h
 *
 *  Created on: 29.10.2012
 *      Author: u4
 */

#ifndef CRYPTBASE_H_
#define CRYPTBASE_H_

#include <enginesound/core/CallBackBase.h>
#include <enginesound/BaseObject.h>

#include <vector>

namespace enginesound
{

class CryptBase: public ErrorObject
{
public:
	typedef std::tr1::shared_ptr<CryptBase> Ptr;

	enum Algorithm
	{
		AES128 = 1, AES256 = 2, DES = 10, TDES = 12
	};

	enum Mode
	{
		ECB = 1, /* ECB mode */
		CBC = 2 /* CBC mode */
	};

	class Session: public ErrorObject
	{
	public:
		typedef std::tr1::shared_ptr<Session> Ptr;

	public:
		Session()
		{

		}
		virtual ~Session()
		{

		}

		virtual bool updateAsync()
		{
			return true;
		}

		virtual bool CheckKey(const Algorithm alg, const Mode mode, const size_t keysize);
	};

	class SessionCrypt: public Session
	{
	public:
		typedef std::tr1::shared_ptr<SessionCrypt> Ptr;

	public:
		virtual bool crypt(unsigned char *result, const unsigned char * input, const size_t size,
				CallBackBase::Ptr callback = CallBackBase::Ptr()) = 0;

		SessionCrypt() :
				Session()
		{

		}
		virtual ~SessionCrypt()
		{

		}
	};

	class SessionDecrypt: public Session
	{
	public:
		typedef std::tr1::shared_ptr<SessionDecrypt> Ptr;

	public:
		virtual bool decrypt(unsigned char *result, const unsigned char * input, const size_t size,
				CallBackBase::Ptr callback = CallBackBase::Ptr()) = 0;

		SessionDecrypt() :
				Session()
		{

		}
		virtual ~SessionDecrypt()
		{

		}
	};

public:
	CryptBase()
	{

	}
	virtual ~CryptBase()
	{

	}

	virtual bool init()
	{
		return true;
	}
	virtual bool close()
	{
		return true;
	}

	virtual SessionCrypt::Ptr createCrypt(const Algorithm alg, const Mode mode, const unsigned char * key,
			const size_t keysize) = 0;
	virtual SessionDecrypt::Ptr createDecrypt(const Algorithm alg, const Mode mode, const unsigned char * key,
			const size_t keysize) = 0;

protected:

};

typedef CryptBase Crypt;

} /* namespace enginesound */

#endif /* CRYPTBASE_H_ */
