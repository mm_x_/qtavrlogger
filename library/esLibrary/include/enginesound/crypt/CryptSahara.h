/*
 * CryptSahara.h
 *
 *  Created on: 01.11.2012
 *      Author: u4
 */

#ifndef CRYPTSAHARA_H_
#define CRYPTSAHARA_H_

#include <enginesound/crypt/CryptBase.h>
#include <string>

#ifdef SAHARA

#include <fsl_shw.h>

namespace enginesound
{

class CryptSahara: public CryptBase
{
public:
	typedef std::tr1::shared_ptr<CryptSahara> Ptr;

	class SessionCrypt: public CryptBase::SessionCrypt
	{
		friend CryptSahara;
	public:
		typedef std::tr1::shared_ptr<SessionCrypt> Ptr;

	public:
		virtual bool crypt(unsigned char *result, const unsigned char * input, const size_t size,
				CallBackBase::Ptr callback = CallBackBase::Ptr());
		virtual ~SessionCrypt();
	protected:
		SessionCrypt(fsl_shw_uco_t* ctx, fsl_shw_pco_t *cap, const fsl_shw_key_alg_t type,
				const fsl_shw_sym_mode_t mode, const unsigned char* data, const size_t size);
	private:
		fsl_shw_uco_t* m_ctx;
		fsl_shw_sko_t * m_shwKey;
		fsl_shw_scco_t * m_symCtx;
	};

	class SessionDecrypt: public CryptBase::SessionDecrypt
	{
		friend CryptSahara;
	public:
		typedef std::tr1::shared_ptr<SessionDecrypt> Ptr;

	public:
		virtual bool decrypt(unsigned char *result, const unsigned char * input, const size_t size,
				CallBackBase::Ptr callback = CallBackBase::Ptr());
		virtual ~SessionDecrypt();

	protected:
		SessionDecrypt(fsl_shw_uco_t* ctx, fsl_shw_pco_t *cap, const fsl_shw_key_alg_t type,
				const fsl_shw_sym_mode_t mode, const unsigned char* data, const size_t size);
	private:
		fsl_shw_uco_t* m_ctx;
		fsl_shw_sko_t * m_shwKey;
		fsl_shw_scco_t * m_symCtx;
	};

public:
	CryptSahara();
	virtual ~CryptSahara();

	virtual bool init();
	virtual bool close();

	virtual CryptBase::SessionCrypt::Ptr createCrypt(const Algorithm alg, const Mode mode, const unsigned char * key,
			const size_t keysize);
	virtual CryptBase::SessionDecrypt::Ptr createDecrypt(const Algorithm alg, const Mode mode,
			const unsigned char * key, const size_t keysize);

protected:
	static fsl_shw_key_alg_t toFSL(const Algorithm type);
	static fsl_shw_sym_mode_t toFSL(const Mode type);

	static fsl_shw_sko_t* getSko(fsl_shw_uco_t* ctx, fsl_shw_pco_t *cap, const fsl_shw_key_alg_t type,
			const fsl_shw_sym_mode_t mode, const unsigned char* data, const size_t size);
	static bool closeSko(fsl_shw_uco_t* ctx, fsl_shw_sko_t* key);

	static fsl_shw_scco_t * getSymCtx(fsl_shw_uco_t* ctx, const fsl_shw_key_alg_t type, const fsl_shw_sym_mode_t mode);
	static void freeSymCtx(fsl_shw_scco_t * ctx);
private:
	fsl_shw_uco_t* m_ctx;
	fsl_shw_pco_t *m_cap;
};

} /* namespace enginesound */

#endif /* SAHARA */
#endif /* CRYPTSAHARA_H_ */
