/*
 * BoostThread.h
 *
 *  Created on: 21.12.2010
 *      Author: user
 */

#ifndef BOOSTTHREAD_H_
#define BOOSTTHREAD_H_

#if defined (__MINGW32__) || defined (_MSC_VER) || defined (_ES_BOOST_THREAD_)
#define _ES_BOOST_THREAD_ 1
#endif

#if _ES_BOOST_THREAD_

#include <enginesound/BaseObject.h>
#include <enginesound/Macros.h>
#include <boost/thread.hpp>

namespace enginesound
{

class BoostThread : public ErrorObject
{
	friend class Functor;
public:
	enum SCHED_POLICY
	{
		UNKNOWN, DEFAULT, ES_RR, ES_FIFO
	};

	class Functor
	{
	public:
		Functor(BoostThread * parent) :
			m_pParent(parent)
		{

		}

		void operator ()()
		{
			m_pParent->m_isWork = true;
			while (m_pParent->m_isStart)
			{
				if (!m_pParent->callback())
					m_pParent->stop();
			}

			m_pParent->m_isWork = false;
		}
	private:
		BoostThread * m_pParent;
	};
public:
	BoostThread(const size_t stackSize = 0);
	virtual ~BoostThread();

	virtual bool init(void);
	virtual bool isInit(void) const
	{
		return true;
	}
	virtual bool close();

	virtual bool start();
	virtual bool stop();
	virtual bool join();

	virtual bool isStart(void) const volatile
	{
		return m_isStart;
	}
	virtual bool isWork(void) const volatile
	{
		return m_isWork;
	}

	virtual int getMinPriority(const SCHED_POLICY ES_UNUSED(policy))
	{
		return 0;
	}
	virtual int getMaxPriority(const SCHED_POLICY ES_UNUSED(policy))
	{
		return 0;
	}

	virtual bool changeScheduling(const SCHED_POLICY policy, const int changePrio);
	virtual bool setMaxPriority(const SCHED_POLICY policy)
	{
		return changeScheduling(policy, getMaxPriority(policy));
	}
	virtual bool setMinPriority(const SCHED_POLICY policy)
	{
		return changeScheduling(policy, getMinPriority(policy));
	}

	static void yeild();
protected:
	int toInt(const SCHED_POLICY policy);
	virtual bool callback() = 0;

	static void * thread_callback(void * args);
private:
	boost::thread * m_pThread;

	volatile bool m_isStart;
	volatile bool m_isWork;
};
}

#endif /* _ES_BOOST_THREAD_ */

#endif /* BOOSTTHREAD_H_ */
