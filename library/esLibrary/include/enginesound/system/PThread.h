/*
 * PThread.h
 *
 *  Created on: 21.12.2010
 *      Author: user
 */

#ifndef PTHREAD_H_
#define PTHREAD_H_

#include <enginesound/BaseObject.h>

#if !defined (_MSC_VER) && !defined (__MINGW32__)
#include <pthread.h>
#include <sched.h>

#include <string>

namespace enginesound
{

class PThread : public ErrorObject
{
public:
	enum SCHED_POLICY
	{
		UNKNOWN, DEFAULT, ES_RR, ES_FIFO
	};

	static const size_t defStackSize = 128 * 1024;
public:
	PThread(const size_t stackSize = defStackSize);
	virtual ~PThread();

	virtual bool init(void);
	virtual bool isInit(void) const
	{
		return true;
	}
	virtual bool close();

	virtual bool start();
	virtual bool stop();
	virtual bool join();

	virtual bool isStart(void) const volatile
	{
		return m_isStart;
	}
	virtual bool isWork(void) const volatile
	{
		return m_isWork;
	}

	virtual int getMinPriority(const SCHED_POLICY policy);
	virtual int getMaxPriority(const SCHED_POLICY policy);

	virtual bool changeScheduling(const SCHED_POLICY policy, const int changePrio);
	virtual bool setMaxPriority(const SCHED_POLICY policy)
	{
		return changeScheduling(policy, getMaxPriority(policy));
	}
	virtual bool setMinPriority(const SCHED_POLICY policy)
	{
		return changeScheduling(policy, getMinPriority(policy));
	}

	static void yeild();
protected:
	int toInt(const SCHED_POLICY policy);
	virtual bool callback() = 0;

	static void * thread_callback(void * args);
private:
	pthread_t m_thread;
	SCHED_POLICY m_curSchedPolicy;

	volatile bool m_isStart;
	bool m_isWork;

	int m_curSchedPriority;

	size_t m_stackSize;
};

}

#endif /* !defined (_MSC_VER) && !defined (__MINGW32__) */

#endif /* PTHREAD_H_ */
