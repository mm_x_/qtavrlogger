/*
 * ThreadCallBack.h
 *
 *  Created on: 12.12.2012
 *      Author: u4
 */

#ifndef THREADCALLBACK_H_
#define THREADCALLBACK_H_

#include <enginesound/system/Thread.h>
#include <boost/tr1/memory.hpp>

namespace enginesound
{

class ThreadCallBack: public Thread
{
public:
	typedef std::tr1::shared_ptr<ThreadCallBack> Ptr;
public:
	class CallBack
	{
	public:
		typedef std::tr1::shared_ptr<CallBack> Ptr;
	public:
		CallBack()
		{

		}
		virtual ~CallBack()
		{

		}

		void setContinue(const bool val = true)
		{
			m_isEnd = !val;
		}
		void setEnd(const bool val = true)
		{
			m_isEnd = val;
		}

		virtual bool isEnd() const
		{
			return m_isEnd;
		}

		virtual void operator()() = 0;
	private:
		bool m_isEnd;
	};

	class CallBackFunc: public CallBack
	{
	public:
		typedef std::tr1::shared_ptr<CallBackFunc> Ptr;
		typedef bool (*callback_func)(void *data);
	public:
		CallBackFunc(callback_func funct, void * data = NULL) :
				m_funct(funct), m_pData(data)
		{

		}
		virtual ~CallBackFunc()
		{

		}

		virtual void operator()()
		{
			if (!(*m_funct)(m_pData))
				setEnd();
			else
				setContinue();
		}
	private:
		callback_func m_funct;
		void * m_pData;
	};
public:
	ThreadCallBack(CallBack::Ptr pCallBack = CallBack::Ptr());
	ThreadCallBack(CallBackFunc::callback_func func, void * data);
	virtual ~ThreadCallBack();

	void setCallBack(CallBack::Ptr pCallBack);
	void setCallBack(CallBackFunc::callback_func func, void * data);

	void setCallBack(bool (*func)(void *data));
protected:
	virtual bool callback();
private:
	CallBack::Ptr m_pCallBack;
};

} /* namespace enginesound */
#endif /* THREADCALLBACK_H_ */
