/*
 * Time.h
 *
 *  Created on: 07.10.2010
 *      Author: user
 */

#ifndef TIME_H_
#define TIME_H_

#include <time.h>
#include <iostream>
#include <iomanip>
#include <assert.h>

namespace enginesound
{

#if defined(_MSC_VER) || defined(__MINGW32__)
class Time
{
	friend ::std::ostream & operator<<(::std::ostream &stream, const Time obj);
public:
	Time();
	Time(const Time & eq)
	: m_time(eq.m_time)
	{

	}
	virtual ~Time()
	{

	}

	static Time getCurrent();

	Time & operator =(const Time & time)
	{
		m_time = time.m_time;
		return *this;
	}
	bool operator ==(const Time & time) const
	{
		return m_time == time.m_time;;
	}
	bool operator <(const Time & time) const
	{
		return m_time < time.m_time;;
	}
	Time operator -(const Time & time) const
	{
		Time retVal;
		retVal.m_time = m_time - time.m_time;
		return retVal;
	}
	Time & operator -=(const Time & time)
	{
		m_time -= time.m_time;
		return *this;
	}
	Time operator +(const Time & time) const
	{
		Time retVal;
		retVal.m_time = m_time + time.m_time;
		return retVal;
	}
	Time & operator +=(const Time & time)
	{
		m_time += time.m_time;
		return *this;
	}

	Time operator /(const size_t num) const
	{
		Time retVal;
		retVal.m_time = m_time / num;
		return retVal;
	}

	Time & operator /=(const size_t num)
	{
		m_time /= num;
		return *this;
	}

	float toFloat()
	{
		return float(m_time) / CLOCKS_PER_SEC;
	}

	long getSecond()
	{
		return m_time / CLOCKS_PER_SEC;
	}
	long getNanoSecond()
	{
		return (m_time - getSecond() * CLOCKS_PER_SEC) * CLOCKS_PER_SEC / 1000000000;
	}
private:
	clock_t m_time;
};

static Time __timeStart = Time::getCurrent();

inline Time getStartTime()
{
	return __timeStart;
}

inline Time getUpTime()
{
	return Time::getCurrent() - getStartTime();
}

inline ::std::ostream & operator<<(::std::ostream &stream, const Time obj)
{
	/* TODO: ����������� */

	return stream;
}

#else /* !defined (_MSC_VER) && !defined (__MINGW32__) */
class Time
{
	friend std::ostream & operator<<(std::ostream &stream, const Time obj);
public:
	Time() :
			m_cur()
	{

	}
	Time(const double dSec)
	{
		m_cur.tv_sec = long(dSec);
		m_cur.tv_nsec = long((dSec - double(m_cur.tv_sec)) * 1000000000.0);
		normalize();
		assert(m_cur.tv_nsec < 1000000000);
	}
	Time(const long int sec, const long int nsec)
	{
		m_cur.tv_sec = sec;
		m_cur.tv_nsec = nsec;
		normalize();
		assert(m_cur.tv_nsec < 1000000000);
	}
	Time(const Time & eq) :
			m_cur(eq.m_cur)
	{
		assert(m_cur.tv_nsec < 1000000000);
	}
	virtual ~Time()
	{

	}

	static Time getCurrent()
	{
		timespec timeCur;
		clock_gettime(CLOCK_REALTIME, &timeCur);

		return Time(timeCur);
	}

	Time & operator =(const Time & time)
	{
		assert(m_cur.tv_nsec < 1000000000);
		assert(time.m_cur.tv_nsec < 1000000000);

		m_cur.tv_sec = time.m_cur.tv_sec;
		m_cur.tv_nsec = time.m_cur.tv_nsec;

		return *this;
	}
	bool operator ==(const Time & time) const
	{
		assert(m_cur.tv_nsec < 1000000000);
		assert(time.m_cur.tv_nsec < 1000000000);

		return ((m_cur.tv_sec == time.m_cur.tv_sec) && (m_cur.tv_nsec == time.m_cur.tv_nsec));
	}
	bool operator <(const Time & time) const
	{
		assert(m_cur.tv_nsec < 1000000000);
		assert(time.m_cur.tv_nsec < 1000000000);

		return (m_cur.tv_sec < time.m_cur.tv_sec)
				|| ((m_cur.tv_sec == time.m_cur.tv_sec) && (m_cur.tv_nsec < time.m_cur.tv_nsec));
	}
	bool operator >(const Time & time) const
	{
		assert(m_cur.tv_nsec < 1000000000);
		assert(time.m_cur.tv_nsec < 1000000000);

		return (m_cur.tv_sec > time.m_cur.tv_sec)
				|| ((m_cur.tv_sec == time.m_cur.tv_sec) && (m_cur.tv_nsec > time.m_cur.tv_nsec));
	}

	Time operator -(const Time & time) const
	{
		assert(time.m_cur.tv_nsec < 1000000000);

		if (m_cur.tv_nsec < time.m_cur.tv_nsec)
		{
			timespec timeRet;
			timeRet.tv_sec = m_cur.tv_sec - time.m_cur.tv_sec - 1;
			timeRet.tv_nsec = (m_cur.tv_nsec + 1000000000) - time.m_cur.tv_nsec;
			return Time(timeRet);
		}
		else
		{
			timespec timeRet;
			timeRet.tv_sec = m_cur.tv_sec - time.m_cur.tv_sec;
			timeRet.tv_nsec = m_cur.tv_nsec - time.m_cur.tv_nsec;
			return Time(timeRet);
		}
	}
	Time & operator -=(const Time & time)
	{
		assert(time.m_cur.tv_nsec < 1000000000);

		if (m_cur.tv_nsec < time.m_cur.tv_nsec)
		{
			m_cur.tv_sec -= time.m_cur.tv_sec + 1;
			m_cur.tv_nsec += 1000000000;
			m_cur.tv_nsec -= time.m_cur.tv_nsec;
		}
		else
		{
			m_cur.tv_sec -= time.m_cur.tv_sec;
			m_cur.tv_nsec -= time.m_cur.tv_nsec;
		}

		assert(m_cur.tv_nsec < 1000000000);
		return *this;
	}
	Time operator +(const Time & time) const
	{
		assert(time.m_cur.tv_nsec < 1000000000);

		timespec timeRet;
		timeRet.tv_sec = m_cur.tv_sec + time.m_cur.tv_sec;
		timeRet.tv_nsec = m_cur.tv_nsec + time.m_cur.tv_nsec;
		if (timeRet.tv_nsec > 1000000000)
		{
			timeRet.tv_nsec -= 1000000000;
			timeRet.tv_sec++;
		}

		assert(m_cur.tv_nsec < 1000000000);
		return Time(time);
	}
	Time & operator +=(const Time & time)
	{
		assert(time.m_cur.tv_nsec < 1000000000);

		m_cur.tv_sec += time.m_cur.tv_sec;
		m_cur.tv_nsec += time.m_cur.tv_nsec;
		if (m_cur.tv_nsec > 1000000000)
		{
			m_cur.tv_nsec -= 1000000000;
			m_cur.tv_sec++;
		}

		assert(m_cur.tv_nsec < 1000000000);
		return *this;
	}

	Time operator /(const size_t num) const
	{
		timespec timeRet;
		timeRet.tv_sec = m_cur.tv_sec / num;
		timeRet.tv_nsec = (m_cur.tv_nsec + m_cur.tv_sec % num * 1000000000) / num;

		assert(m_cur.tv_nsec < 1000000000);
		return Time(timeRet);
	}

	Time & operator /=(const size_t num)
	{
		m_cur.tv_sec /= num;
		m_cur.tv_nsec += m_cur.tv_sec % num * 1000000000;
		m_cur.tv_nsec /= num;

		assert(m_cur.tv_nsec < 1000000000);
		return *this;
	}

	float toFloat() const
	{
		assert(m_cur.tv_nsec < 1000000000);
		return float(m_cur.tv_sec) + float(m_cur.tv_nsec) / 1000000000;
	}

	long getSecond() const
	{
		return m_cur.tv_sec;
	}
	long getNanoSecond() const
	{
		assert(m_cur.tv_nsec < 1000000000);
		return m_cur.tv_nsec;
	}
protected:
	Time(const timespec & time) :
			m_cur(time)
	{
		assert(m_cur.tv_nsec < 1000000000);
	}
	Time & operator =(const timespec & time)
	{
		m_cur.tv_sec = time.tv_sec;
		m_cur.tv_nsec = time.tv_nsec;
		assert(m_cur.tv_nsec < 1000000000);

		return *this;
	}

	void normalize()
	{
		while (m_cur.tv_nsec >= 1000000000)
		{
			m_cur.tv_nsec--;
			m_cur.tv_sec++;
		}

		assert(m_cur.tv_nsec < 1000000000);
	}
private:
	timespec m_cur;
};

static Time __timeStart = Time::getCurrent();

inline Time getStartTime()
{
	return __timeStart;
}

inline Time getUpTime()
{
	return Time::getCurrent() - getStartTime();
}

inline std::ostream & operator<<(std::ostream &stream, const Time obj)
{
	stream << obj.m_cur.tv_sec;
	stream << '.';
	stream << std::setfill('0') << std::setw(9) << obj.m_cur.tv_nsec;

	return stream;
}

#endif /* !defined (_MSC_VER) && !defined (__MINGW32__) */
}

#endif /* TIME_H_ */
