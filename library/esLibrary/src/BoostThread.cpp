/*
 * BoostThread.cpp
 *
 *  Created on: 21.12.2010
 *      Author: user
 */

#include <enginesound/system/BoostThread.h>

namespace enginesound
{

#ifdef _ES_BOOST_THREAD_

BoostThread::BoostThread(const size_t ES_UNUSED(stackSize)) :
		m_pThread(NULL), m_isStart(false), m_isWork(false)
{

}

BoostThread::~BoostThread()
{
	stop();
	if (m_pThread)
		m_pThread->join();
	close();
}

bool BoostThread::init(void)
{
	return true;
}

bool BoostThread::close()
{
	delete m_pThread;
	m_pThread = NULL;

	return true;
}

bool BoostThread::start()
{
	Functor functor(this);
	try
	{
		m_pThread = new boost::thread(functor);
	} catch (std::exception & exc)
	{
		setErrorMsg(exc.what());

		delete m_pThread;
		m_pThread = NULL;
		return false;
	}

	m_isStart = true;

	return true;
}
bool BoostThread::stop()
{
	if (m_pThread == NULL)
		return true;

	m_isStart = false;

	return true;
}

bool BoostThread::join()
{
	if (m_pThread == NULL)
	{
		setErrorMsg("Thread not started");
		return false;
	}

	try
	{
		m_pThread->join();
	} catch (std::exception & exc)
	{
		setErrorMsg(exc.what());

		return false;
	}

	return true;
}

bool BoostThread::changeScheduling(const SCHED_POLICY ES_UNUSED(policy), const int ES_UNUSED(changePrio))
{
	setErrorMsg("operation not supported");
	return false;
}

void BoostThread::yeild()
{
	boost::thread::yield();
}

#endif /* _ES_BOOST_THREAD_ */

}
