/*
 * CryptBase.cpp
 *
 *  Created on: 06.11.2012
 *      Author: u4
 */

#include <enginesound/crypt/CryptBase.h>

namespace enginesound
{

bool CryptBase::Session::CheckKey(const Algorithm alg, const Mode mode, const size_t keysize)
{
	switch (alg)
	{
	case AES128:
		if (keysize != 16)
		{
			setErrorMsg("AES128 not support key length %d", keysize);
			return false;
		}

		break;
	case AES256:
		if (keysize != 32)
		{
			setErrorMsg("AES256 not support key length %d", keysize);
			return false;
		}
		break;
	case DES:
		if (keysize != 7)
		{
			setErrorMsg("DES not support key length %d", keysize);
			return false;
		}
		break;
	case TDES:
		if (keysize != 7 * 3)
		{
			setErrorMsg("Triple DES not support key length %d", keysize);
			return false;
		}
		break;
	default:
		setErrorMsg("Algorithm not supported");
		return false;
	}

	switch (mode)
	{
	case ECB:
	case CBC:
		break;
	default:
		setErrorMsg("Mode not supported");
		return false;
	}

	return true;
}

}  // namespace enginesound
