/*
 * CryptLinux.cpp
 *
 *  Created on: 29.10.2012
 *      Author: u4
 */

#include <enginesound/crypt/CryptLinux.h>

#ifdef DEVCRYPTOLINUX

using namespace std::tr1;

namespace enginesound
{

CryptLinux::CryptLinux() :
		m_fd(-1)
{
}

CryptLinux::~CryptLinux()
{
	if (m_fd != -1)
		close();
}

bool CryptLinux::init()
{
	if (!CryptBase::init())
		return false;

	/* Open the crypto device */
	m_fd = ::open("/dev/crypto", O_RDWR, 0);
	if (m_fd == -1)
	{
		setErrorMsg("open(/dev/crypto)");
		return false;
	}

	return true;
}

bool CryptLinux::close()
{
	CryptBase::close();

	if (m_fd == -1)
	{
		setErrorMsg("/dev/crypto not open");
		return false;
	}

	/* Close the original descriptor */
	if (::close(m_fd))
	{
		setErrorMsg("close(fd)");
		return false;
	}

	return true;
}

CryptBase::SessionCrypt::Ptr CryptLinux::createCrypt(const Algorithm alg, const Mode mode, const unsigned char * key,
		const size_t keysize)
{
	int cfd = -1;
	if (m_fd == -1)
	{
		setErrorMsg("/dev/crypto not open");
		return SessionCrypt::Ptr();
	}

	/* Clone file descriptor */
	if (ioctl(m_fd, CRIOGET, &cfd))
	{
		perror("ioctl(CRIOGET)");
		return SessionCrypt::Ptr();
	}

	/* Set close-on-exec (not really neede here */
	if (fcntl(cfd, F_SETFD, 1) == -1)
	{
		perror("fcntl(F_SETFD)");
		return SessionCrypt::Ptr();
	}

	return SessionCrypt::Ptr(new SessionCrypt(cfd, alg, mode, key, keysize));
}
CryptBase::SessionDecrypt::Ptr CryptLinux::createDecrypt(const Algorithm alg, const Mode mode,
		const unsigned char * key, const size_t keysize)
{
	int cfd = -1;
	if (m_fd == -1)
	{
		setErrorMsg("/dev/crypto not open");
		return SessionDecrypt::Ptr();
	}

	/* Clone file descriptor */
	if (ioctl(m_fd, CRIOGET, &cfd))
	{
		perror("ioctl(CRIOGET)");
		return SessionDecrypt::Ptr();
	}

	/* Set close-on-exec (not really neede here */
	if (fcntl(cfd, F_SETFD, 1) == -1)
	{
		perror("fcntl(F_SETFD)");
		return SessionDecrypt::Ptr();
	}

	return SessionDecrypt::Ptr(new SessionDecrypt(cfd, alg, mode, key, keysize));
}

CryptLinux::SessionCrypt::SessionCrypt(int fd, const Algorithm alg, const Mode mode, const unsigned char * key,
		const size_t keysize) :
		CryptBase::SessionCrypt(), m_fd(fd), m_session(NULL), m_iv(NULL)
{
	const size_t sizeIV = CryptLinux::getIVSize(alg, mode);
	if (sizeIV > 0)
	{
		m_iv = new char[sizeIV];
		memset(m_iv, 0, sizeIV);
	}

	m_session = CryptLinux::setKey(fd, alg, mode, key, keysize);
}
CryptLinux::SessionCrypt::~SessionCrypt()
{
	CryptLinux::closeSession(m_fd, m_session);

	delete[] m_iv;
}

bool CryptLinux::SessionCrypt::crypt(unsigned char *result, const unsigned char * input, const size_t size,
		CallBackBase::Ptr callback)
{
	struct crypt_op cryp;

	memset(&cryp, 0, sizeof(cryp));

	/* Encrypt data.in to data.encrypted */
	cryp.ses = m_session->ses;
	cryp.len = size;
	cryp.src = (char*) input;
	cryp.dst = (char*) result;
	cryp.iv = m_iv;
	cryp.op = COP_ENCRYPT;
	if (ioctl(m_fd, CIOCCRYPT, &cryp))
	{
		setErrorMsg("ioctl(CIOCCRYPT)");
		return false;
	}

	if (size >= 16)
		memcpy(m_iv, result + size - 16, 16);

	if (callback)
		(*callback)();

	return true;
}

CryptLinux::SessionDecrypt::SessionDecrypt(int fd, const Algorithm alg, const Mode mode, const unsigned char * key,
		const size_t keysize) :
		CryptBase::SessionDecrypt(), m_fd(fd), m_session(NULL), m_iv(NULL)
{
	const size_t sizeIV = CryptLinux::getIVSize(alg, mode);
	if (sizeIV > 0)
	{
		m_iv = new char[sizeIV];
		memset(m_iv, 0, sizeIV);
	}

	m_session = CryptLinux::setKey(fd, alg, mode, key, keysize);
}
CryptLinux::SessionDecrypt::~SessionDecrypt()
{
	CryptLinux::closeSession(m_fd, m_session);

	delete[] m_iv;
}

bool CryptLinux::SessionDecrypt::decrypt(unsigned char *result, const unsigned char * input, const size_t size,
		CallBackBase::Ptr callback)
{
	struct crypt_op cryp;

	memset(&cryp, 0, sizeof(cryp));

	/* Encrypt data.in to data.encrypted */
	cryp.ses = m_session->ses;
	cryp.len = size;
	cryp.src = (char*) input;
	cryp.dst = (char*) result;
	cryp.iv = m_iv;
	cryp.op = COP_DECRYPT;
	if (ioctl(m_fd, CIOCCRYPT, &cryp))
	{
		setErrorMsg("ioctl(CIOCCRYPT)");
		return false;
	}

	if (size >= 16)
		memcpy(m_iv, input + size - 16, 16);

	if (callback)
		(*callback)();

	return true;
}

struct session_op* CryptLinux::setKey(int fd, const Algorithm alg, const Mode mode, const unsigned char * key,
		const size_t keysize)
{
	char * nameAES = "aes";
	struct session_op* session = new struct session_op;
	memset(session, 0, sizeof(*session));

	switch (alg)
	{
	case Crypt::AES128:
	case Crypt::AES256:
		if (mode == Crypt::CBC)
		{
			session->cipher = CRYPTO_CIPHER_NAME | CRYPTO_FLAG_CBC;
			session->alg_name = nameAES;
			session->alg_namelen = strlen(nameAES);
		}
		else if (mode == Crypt::ECB)
		{
			session->cipher = CRYPTO_CIPHER_NAME | CRYPTO_FLAG_ECB;
			session->alg_name = nameAES;
			session->alg_namelen = strlen(nameAES);
		}
		break;
	default:
		delete session;
		return NULL;
	}

	char * keyCopy = new char[keysize];
	memcpy(keyCopy, key, keysize);
	session->keylen = keysize;
	session->key = keyCopy;

	if (ioctl(fd, CIOCGSESSION, session))
	{
		delete[] keyCopy;
		delete session;
		return NULL;
	}

	return session;
}

void CryptLinux::closeSession(int fd, struct session_op* session)
{
	/*Finish crypto session */
	if (ioctl(fd, CIOCFSESSION, session->ses))
	{
		/* error */
	}

	delete[] session->key;
	delete session;
}

size_t CryptLinux::getIVSize(const Algorithm alg, const Mode mode)
{
	size_t sizeIv;
	switch (alg)
	{
	case Crypt::AES128:
		sizeIv = 16;
		break;
	case Crypt::AES256:
		sizeIv = 32;
		break;
	default:
		sizeIv = 0;
		break;
	}

	return sizeIv;
}

} /* namespace enginesound */

#endif /* DEVCRYPTOLINUX */

