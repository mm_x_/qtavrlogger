/*
 * ErrorObject.cpp
 *
 *  Created on: 21.11.2011
 *      Author: user
 */

#include <enginesound/core/ErrorObject.h>
#include <cstdio>
#include <cstdarg>

using namespace std;

namespace enginesound
{

void ErrorObject::setErrorMsg(const std::string &format, ...)
{
	boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(m_mutexErrorMsg);
	va_list args;

	const char * str = format.c_str();
	va_start(args, str);
	setErrorMsg(str, args);
	va_end(args);
}

void ErrorObject::setErrorMsg(const char * format, ...)
{
	boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(m_mutexErrorMsg);
	va_list args;

	va_start(args, format);
	setErrorMsg(format, args);
	va_end(args);
}

void ErrorObject::setErrorMsg(const char * format, va_list args)
{
	char * buff;
	int size_buff = 256;
	do
	{
		buff = new char[size_buff];
		int size_do = vsnprintf(buff, size_buff, format, args);
		if (size_do == -1)
			break;

		if ((size_do + 1) > size_buff)
		{
			delete[] buff;
			size_buff = size_do + 1;
		}
		else
			break;
	} while (1);

	m_stringError = buff;
}

} /* namespace enginesound */
