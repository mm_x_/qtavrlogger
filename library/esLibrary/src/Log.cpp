/*
 * Log.cpp
 *
 *  Created on: 19.07.2010
 *      Author: user2
 */

#include <enginesound/log/Log.h>
#include <enginesound/log/LogStd.h>
#include <cstdio>

#include <boost/thread/mutex.hpp>

using namespace std;

namespace enginesound
{

Log::arrayLogObject & getGlobalLogObjList()
{
	static Log::arrayLogObject obj = Log::arrayLogObject(1, LogBase::Ptr(new LogStd()));
	return obj;
}

boost::mutex & getGlobalLogMutex()
{
	static boost::mutex mutex;
	return mutex;
}

Log::Log()
{

}

Log::~Log()
{

}

void Log::info(const char * format, ...)
{
	va_list args;
	va_start(args, format);
	vlog(0, format, args);
	va_end(args);
}

void Log::warning(const char * format, ...)
{
	va_list args;
	va_start(args, format);
	vlog(1, format, args);
	va_end(args);
}

void Log::error(const char * format, ...)
{
	va_list args;
	va_start(args, format);
	vlog(2, format, args);
	va_end(args);
}

void Log::critical_error(const char * format, ...)
{
	va_list args;
	va_start(args, format);
	vlog(3, format, args);
	va_end(args);
}

void Log::message(const char * format, ...)
{
	va_list args;
	va_start(args, format);
	vlog(4, format, args);
	va_end(args);
}

void Log::log(const int level, const char * format, ...)
{
	va_list args;
	va_start(args, format);
	vlog(level, format, args);
	va_end(args);
}

void Log::vlog(const int level, const char * format, va_list arg)
{
	if (level < getLevel())
		return;

	char * buff;
	int size_buff = 256;
	do
	{
		buff = new char[size_buff];
		int size_do = vsnprintf(buff, size_buff, format, arg);
		if (size_do == -1)
			break;

		if ((size_do + 1) > size_buff)
		{
			delete[] buff;
			size_buff = size_do + 1;
		}
		else
			break;
	} while (1);

	{
		getGlobalLogMutex().lock();
		for (arrayLogObject::iterator pos = getLogObjList().begin(); pos != getLogObjList().end(); ++pos)
			(*pos)->log(level, buff);
		getGlobalLogMutex().unlock();
	}

	delete[] buff;
}

std::string vmessageToString(const char * format, ...)
{
	va_list args;
	va_start(args, format);
	const std::string retval = vmessageToString(format, args);
	va_end(args);
	return retval;
}

std::string vmessageToString(const char * format, std::va_list arg)
{
	char * buff;
	int size_buff = 256;
	do
	{
		buff = new char[size_buff];
		int size_do = vsnprintf(buff, size_buff, format, arg);
		if (size_do == -1)
			break;

		if ((size_do + 1) > size_buff)
		{
			delete[] buff;
			size_buff = size_do + 1;
		}
		else
			break;
	} while (1);

	std::string retString(buff);
	delete[] buff;

	return retString;
}

void Log::addLog(LogBase::Ptr obj)
{
	getLogObjList().push_back(obj);
}
void Log::delLog(LogBase::Ptr obj)
{
	getLogObjList().remove(obj);
}

void Log::setLevel(const int level)
{
	getLevelLocal() = level;
}
int Log::getLevel(void)
{
	return getLevelLocal();
}

int & Log::getLevelLocal(void)
{
	static int level = 0;
	return level;
}

void Log::clearLogList()
{
	getLogObjList().clear();
}

Log::arrayLogObject & Log::getLogObjList()
{
	return getGlobalLogObjList();
}

}
