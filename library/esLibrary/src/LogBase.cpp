/*
 * LogBase.cpp
 *
 *  Created on: 27.01.2010
 *      Author: user
 */

#include "enginesound/log/LogBase.h"

namespace enginesound
{

Time LogBase::m_timeStart = Time::getCurrent();

LogBase::LogBase(const size_t minLevel) :
	m_minLevel(minLevel)
{

}

LogBase::~LogBase()
{

}

void LogBase::log(const size_t, const char *)
{

}

}
