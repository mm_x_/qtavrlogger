/*
 * LogStd.cpp
 *
 *  Created on: 19.07.2010
 *      Author: user2
 */

#include "enginesound/log/LogStd.h"
#include <iostream>
#include <iomanip>
#include <time.h>

using namespace std;

namespace enginesound
{

LogStd::LogStd(const size_t minLevel) :
	LogBase(minLevel)
{

}

LogStd::~LogStd()
{
}

void LogStd::log(const size_t level, const char * msg)
{
	if (level >= getMinLevel())
	{
		const Time timeCur = getUpTime();

		if (level < 1)
			cout << timeCur << ' ' << msg << endl;
		else
			cerr << timeCur << ' ' << msg << endl;
	}
}

}
