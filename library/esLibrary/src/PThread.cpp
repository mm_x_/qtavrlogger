/*
 * PThread.cpp
 *
 *  Created on: 21.12.2010
 *      Author: user
 */

#include <enginesound/system/PThread.h>
#include <sstream>
#include <cstring>

using namespace std;

namespace enginesound
{

#if !defined (_MSC_VER) && !defined (__MINGW32__)
PThread::PThread(const size_t stackSize) :
		m_thread(), m_curSchedPolicy(UNKNOWN), m_isStart(false), m_isWork(false), m_curSchedPriority(), m_stackSize(
				stackSize)
{

}

PThread::~PThread()
{
	stop();
	if (m_thread != 0 && isWork())
		join();

	close();

	if (m_thread != 0)
		pthread_detach(m_thread);
}

bool PThread::init(void)
{
	if (m_thread != 0)
	{
		setErrorMsg("Thread already used");
		m_isStart = false;
		return false;
	}
	return true;
}

bool PThread::start()
{
	int err = 0;
	m_isStart = true;

	pthread_attr_t attr;
	err = pthread_attr_init(&attr);

	/* setting a new size */
	int stacksize = int(m_stackSize);
	err = pthread_attr_setstacksize(&attr, stacksize);
	if (err != 0)
	{
		setErrorMsg("Can`t set stack size %d, err: %s", stacksize, strerror(err));
		m_isStart = false;
		return false;
	}

	if ((err = pthread_create(&m_thread, &attr, thread_callback, this)) != 0)
	{
		setErrorMsg("Can`t create thread, err: %s", strerror(err));
		m_isStart = false;
		return false;
	}

	if (m_curSchedPolicy != UNKNOWN)
		changeScheduling(m_curSchedPolicy, m_curSchedPriority);

	return true;
}

bool PThread::close()
{
	return !isWork();
}

bool PThread::stop()
{
	if (!m_isStart)
		return true;

	m_isStart = false;

	return true;
}

bool PThread::join()
{
	int err = 0;
	if (m_thread == 0)
	{
		setErrorMsg("Thread not started");
		return false;
	}

	if ((err = pthread_join(m_thread, NULL)) != 0)
	{
		setErrorMsg("Can`t join thread, err: %s", strerror(err));
		return false;
	}

	return true;
}

void *PThread::thread_callback(void *args)
{
	PThread *obj = (PThread*) (args);
	obj->m_isWork = true;
	while (obj->m_isStart)
	{
		if (!obj->callback())
			obj->stop();
	}

	obj->m_isWork = false;

	return NULL;
}

int PThread::getMinPriority(const PThread::SCHED_POLICY policy)
{
	return sched_get_priority_min(toInt(policy));
}
int PThread::getMaxPriority(const PThread::SCHED_POLICY policy)
{
	return sched_get_priority_max(toInt(policy));
}

bool PThread::changeScheduling(const PThread::SCHED_POLICY policy, const int changePrio)
{
	struct sched_param param;
	memset(&param, 0, sizeof(param));
	param.sched_priority = changePrio;

	m_curSchedPolicy = policy;
	m_curSchedPriority = changePrio;

	if (m_curSchedPriority < getMinPriority(policy))
	{
		setErrorMsg("priority less then minimum volume - %d", getMinPriority(policy));
		return false;
	}

	if (m_curSchedPriority > getMaxPriority(policy))
	{
		setErrorMsg("priority more then maximum volume - %d", getMaxPriority(policy));
		return false;
	}

	if (!m_isStart)
		return true;

	int rc = pthread_setschedparam(m_thread, toInt(policy), &param);
	if (rc != 0)
	{
		setErrorMsg("Can`t set schedparam, err: %s", strerror(rc));
		return false;
	}

	return true;
}

int PThread::toInt(const PThread::SCHED_POLICY policy)
{
	switch (policy)
	{
	case ES_RR:
		return SCHED_RR;
	case ES_FIFO:
		return SCHED_FIFO;
	default:
		return SCHED_OTHER;
	}
}

void PThread::yeild()
{
	pthread_yield();
}

#endif /* !defined (_MSC_VER) && !defined (__MINGW32__) */

}
