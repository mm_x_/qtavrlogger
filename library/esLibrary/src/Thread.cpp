/*
 * Thread.cpp
 *
 *  Created on: 14.03.2011
 *      Author: u121
 */

#include <enginesound/system/Thread.h>
#if (_ES_BOOST_THREAD_)
#include <boost/thread.hpp>
#elif defined(__unix__)
#include <unistd.h>
#elif defined (_MSC_VER) || defined (__MINGW32__)
#include <windows.h>
#endif

#include <boost/version.hpp>
#if BOOST_VERSION < 105000
#define TIME_UTC_ TIME_UTC
#endif

using namespace boost;

namespace enginesound
{

#if (_ES_BOOST_THREAD_)
void msleep(const size_t msec)
{
	boost::xtime xt;
	boost::xtime_get(&xt, TIME_UTC_);
	size_t mseclocal = msec;
	size_t sec = 0;
	for (; mseclocal >= 1000; sec++)
		mseclocal -= 1000;

	xt.nsec += mseclocal * 1000000;
	xt.sec += sec;

	boost::thread::sleep(xt);
}
#elif defined(__unix__)
void msleep(const size_t msec)
{
	// TODO: fix more than few seconds
	usleep(msec * 1000);
}
#elif defined (_MSC_VER) || defined (__MINGW32__)
void msleep(const size_t msec)
{
	Sleep(msec);
}
#endif


}
