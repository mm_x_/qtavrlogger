/*
 * ThreadCallBack.cpp
 *
 *  Created on: 12.12.2012
 *      Author: u4
 */

#include <enginesound/system/ThreadCallBack.h>

namespace enginesound
{

ThreadCallBack::ThreadCallBack(CallBack::Ptr pCallBack) :
		Thread(), m_pCallBack(pCallBack)
{

}

ThreadCallBack::~ThreadCallBack()
{

}

ThreadCallBack::ThreadCallBack(CallBackFunc::callback_func func, void* data) :
		Thread(), m_pCallBack(CallBackFunc::Ptr(new CallBackFunc(func, data)))

{
}

void ThreadCallBack::setCallBack(CallBack::Ptr pCallBack)
{
	m_pCallBack = pCallBack;
}

void ThreadCallBack::setCallBack(CallBackFunc::callback_func func, void* data)
{
	setCallBack(CallBackFunc::Ptr(new CallBackFunc(func, data)));
}

bool ThreadCallBack::callback()
{
	if (!m_pCallBack)
		return false;

	(*m_pCallBack)();

	return !m_pCallBack->isEnd();
}

} /* namespace enginesound */
