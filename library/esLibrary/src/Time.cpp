/*
 * Time.cpp
 *
 *  Created on: 23.12.2010
 *      Author: user
 */

#include <enginesound/system/Time.h>

namespace enginesound
{
#if defined(_MSC_VER) || defined(__MINGW32__)

Time::Time() 
	: m_time()
{
}

Time Time::getCurrent()
{
	Time retVal;
	retVal.m_time = clock(); 
	return retVal;
}

#endif /* defined(_MSC_VER) || defined(__MINGW32__) */
}
