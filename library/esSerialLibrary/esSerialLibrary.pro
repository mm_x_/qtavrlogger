#-------------------------------------------------
#
# Project created by QtCreator 2013-05-16T13:30:40
#
#-------------------------------------------------

QT       -= core gui

TARGET = esSerialLibrary
TEMPLATE = lib
CONFIG += staticlib
INCLUDEPATH += ./include/

SOURCES += \
    src/SerialPort.cpp

HEADERS += \
    include/esSerialPort.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
