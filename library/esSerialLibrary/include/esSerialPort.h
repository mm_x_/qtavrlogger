/*
 * esSerialControl.h
 *
 *  Created on: 11.02.2011
 *      Author: u121
 */

#ifndef ESSERIALPORT_H_
#define ESSERIALPORT_H_

#include <stdlib.h>
#include <stdint.h>

#if _MSC_VER < 1600
typedef __int8 int8_t;
typedef __int16 int16_t;
typedef __int32 int32_t;

typedef unsigned __int8 uint8_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;
#else
#include <stdint.h>
#endif /* _MSC_VER */

#ifdef __cplusplus
extern "C"
{
#endif	/* __cplusplus */

/*
 * TODO : Реализовать флаг ошибки(успеха)
 * TODO : Realize error flags
 */

/*
 * Packet:
 * 	0 - frameStart
 * 	1 - Bits command
 * 	2 - Code command
 * 	(3,4,6) - Data (little-endian, integer(float) )
 * 	3(4,5,7) - frameStop
 *  4(5,6,8) - crc8 (only for command and data), zero (if not support crc8)
 *
 * Bits Command:
 * 	7 -> CRC (0 - without CRC8 \ 1 - with CRC8)
 * 	6 -> Type (0 - Get \ 1 - Put)
 * 	3-5 -> Reserved
 * 	0-2 -> Length data (0 - Zero, 01 - Byte, 10 - Word(16 bit), 11 - Float(32 bit) )
 *
 * Code Command:
 *  7 -> 0 - Data, 1 - Command
 *	6 -> Reserved
 * 	if(bit 7 == 0):
 *  	5 -> 0 - ADC, 1 - FREQ
 *  	4 -> 1 - NORM / 0 - NON NORM
 *
 *  	0-3 -> NUM CHANNEL (except 0000 - NONE)
 *
 *  TODO:
 * 	if(bit 7 == 1) :
 * 		if(bit 6 == 0)
 *  		000000 - NONE
 *  		000001 - VERSION
 *  		000010 - ACTIVE / PASSIVE MODE
 *  		000011 - NORMAL / TEST MODE
 *  		001000 - TICK ( OUT = FREQ / TICK)
 *  		100000 - ENABLE ADC CHANNEL
 *  		100001 - DISABLE ADC CHANNEL
 *  		110000 - ENABLE FREQ CHANNEL
 *  		110001 - DISABLE FREQ CHANNEL
 *
 *  11111111 - ERROR
 */

#define esUART_VER 			0x04

#define esEnable 1
#define esDisable 0

#define esE_OK 				0
#define esE_UNKNOWN 		-1
#define esE_NOTSUPPORTED 	-2
#define esE_BUSY 			-3

#define esCMDTypeData 1
#define esCMDTypeOther 0

#define esCMDTypePos 6
#define esCMDTypeMask 0x40

#define esCMDDataEnablePos 4
#define esCMDDataEnableMask 0x10

#define esCMD_VERSION 0x01
// TODO: ACTIVE MODE
#define esCMD_ACTIVE 0x02
#define esCMD_TEST	0x03
#define esCMD_FREQ_TICK 0x8
#define esCMD_ENABLE_ADC_CH 0x20
#define esCMD_DISABLE_ADC_CH 0x21
// TODO: ENABLE / DISABLE FREQ
#define esCMD_ENABLE_FREQ_CH 0x30
#define esCMD_DISABLE_FREQ_CH 0x31

#define esSFrameInterlive 0x00
#define esSFrameStart 0x7f
#define esSFrameStop 0xff

#define esSBitsPosCRC 7
#define esSBitsPosType 6
#define esSBitsPosLength 0

#define esSBitsMaskCRC 0x80
#define esSBitsMaskType 0x40
#define esSBitsMaskLength 0x07

#define esSCodePosCommand 7
#define esSCodePosFreq 5
#define esSCodePosNorm 4
#define esSCodePosNum 0

#define esSCodeMaskCommand 0x80
#define esSCodeMaskADCFreq 0x20
#define esSCodeMaskNorm 0x10
#define esSCodeMaskNum 0x0F
#define esSCodeMaskCode 0x3F

#define esSPut 0x01
#define esSGet 0x00

#define esSZero 0x00
#define esSByte 0x01
#define esSWord 0x02
#define esSFloat 0x03

#define esSEnableCRC 0x01
#define esSDisableCRC 0x00

#define esSADC 0x00
#define esSFreq 0x01

#define esSNorm 0x01
#define esSNotNorm 0x00

#define esSCodeNone 0x00
#define esSCodeError 0xFF

#define esSFlowNone 0
#define esSFlowSoftware 1
#define esSFlowHardware 2

#define esSParityNone 0
#define esSParityOdd 1
#define esSParityEven 2

#define esSStopOne 1
#define esSStopOnePointFive 15
#define esSStopTwo 2

#define esSMinLengthPacket 5
#define esSMaxLengthPacket 9

struct esSerialControl;

/*
 Name  : CRC-8
 Poly  : 0x31    x^8 + x^5 + x^4 + 1
 Init  : 0xFF
 Revert: false
 XorOut: 0x00
 Check : 0xF7 ("123456789")
 MaxLen: 15 байт(127 бит) - обнаружение
 - одинарных, двойных, тройных и всех нечетных ошибок
 */
unsigned char esSCrc8(unsigned char *pcBlock, unsigned int length);

struct esSerialControl * esSInit();
int esSOpen(struct esSerialControl * control, char * pathRS232);
void esSClose(struct esSerialControl * control);
void esSClear(struct esSerialControl * control);

int esSSetBaudRate(struct esSerialControl * control, uint32_t baud);
int esSSetCharSize(struct esSerialControl * control, int size);
int esSSetFlow(struct esSerialControl * control, int flow);
int esSSetParity(struct esSerialControl * control, int parity);
int esSSetStop(struct esSerialControl * control, int stop);

int esSGetData(struct esSerialControl * control, unsigned char * buff, unsigned int size, int timeout = 0);

int esSSendGetData(struct esSerialControl * control, char command, int type = esSFloat, int withCRC8 = esSEnableCRC);
int esSSendGetFreq(struct esSerialControl * control, int channel, char is_norm = 0, int type = esSFloat, int withCRC8 =
		esSEnableCRC);
int esSSendGetADC(struct esSerialControl * control, int channel, char is_norm = 0, int type = esSFloat,
		int withCRC8 = esSEnableCRC);
int esSSendCMDGet(struct esSerialControl * control, char command, int type = esSWord, int withCRC8 = esSEnableCRC);

int esSSendPutData(struct esSerialControl * control, char command, void * data, size_t lendata, int withCRC8 = esSEnableCRC);
int esSSendError(struct esSerialControl * control, short err_no = -1, int withCRC8 = esSEnableCRC);

int esSSendEnableFreq(struct esSerialControl * control, uint8_t channel, bool enable = true, int withCRC8 = esSEnableCRC);
int esSSendFreq(struct esSerialControl * control, int channel, void * data, size_t lendata, char is_norm = 0,
		int withCRC8 = esSEnableCRC);
int esSSendFreqByte(struct esSerialControl * control, int channel, char freq, char is_norm = 0, int withCRC8 = esSEnableCRC);
int esSSendFreqWord(struct esSerialControl * control, int channel, int16_t freq, char is_norm = 0, int withCRC8 = esSEnableCRC);
int esSSendFreqFloat(struct esSerialControl * control, int channel, float freq, char is_norm = 0, int withCRC8 = esSEnableCRC);

int esSSendEnableADC(struct esSerialControl * control, uint8_t channel, bool enable = true, int withCRC8 = esSEnableCRC);
int esSSendADC(struct esSerialControl * control, int channel, void * data, size_t lendata, char is_norm = 0,
		int withCRC8 = esSEnableCRC);
int esSSendADCByte(struct esSerialControl * control, int channel, char vol, char is_norm = 0, int withCRC8 = esSEnableCRC);
int esSSendADCWord(struct esSerialControl * control, int channel, int16_t vol, char is_norm = 0, int withCRC8 = esSEnableCRC);
int esSSendADCFloat(struct esSerialControl * control, int channel, float vol, char is_norm = 0, int withCRC8 = esSEnableCRC);

int esSSendCMDByte(struct esSerialControl * control, char command, char data, int withCRC8 = esSEnableCRC);
int esSSendCMDWord(struct esSerialControl * control, char command, short data, int withCRC8 = esSEnableCRC);
int esSSendCMDFloat(struct esSerialControl * control, char command, float data, int withCRC8 = esSEnableCRC);

int esSParse(struct esSerialControl * control, unsigned char * buff, unsigned int size);

char * esSGetLastErrorMsg(struct esSerialControl * control);

void esSetCallBackSendCommand(struct esSerialControl * control, void (*send)(struct esSerialControl*, int, int, void *),
		void * client = 0);
void esSetCallBackSendFreq(struct esSerialControl * control,
		void (*send)(struct esSerialControl*, int channel, int is_norm, int type_arg, void * client),
		void * client = 0);
void esSetCallBackSendADC(struct esSerialControl * control,
		void (*send)(struct esSerialControl*, int channel, int is_norm, int type_arg, void * client),
		void * client = 0);

void esSetCallBackReceiveCommand(struct esSerialControl * control,
		void (*put)(struct esSerialControl*, int, void *, int, void *), void * client = 0);
void esSetCallBackReceiveFreq(struct esSerialControl * control,
		void (*put)(struct esSerialControl*, int channel, int is_norm, void * buff, int type_arg, void * client),
		void * client = 0);
void esSetCallBackReceiveADC(struct esSerialControl * control,
		void (*put)(struct esSerialControl*, int channel, int is_norm, void * buff, int type_arg, void * client),
		void * client = 0);

void esSetCallBackReceiveError(struct esSerialControl * control,
		void (*put)(struct esSerialControl*, void *, int, void *), void * client = 0);

int esSUpdate(struct esSerialControl * control, unsigned int max_time);

#ifdef __cplusplus
} /* extern "C" */
#endif	/* __cplusplus */

#endif /* ESSERIALPORT_H_ */
