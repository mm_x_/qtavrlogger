/*
 * SerialPort.c
 *
 *  Created on: 01.03.2011
 *      Author: u121
 */

#include <esSerialPort.h>

#include <iostream>
#include <cerrno>

#include <boost/asio.hpp>
#include <boost/bind.hpp>

#ifdef _MSC_VER
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;
#endif /* _MSC_VER */

using namespace boost::asio;
using namespace std;

#define buff_size_max 256

#define htons_g(a)            ((((a)>>8)&0xff)|(((a)<<8)&0xff00))
#define ntohs_g(a)            htons(a)

#define htonl_g(a)            ( (((a)>>24)&0xff) | (((a)>>8)&0xff00) |\
                                (((a)<<8)&0xff0000) | (((a)<<24)&0xff000000) )
#define ntohl_g(a)            htonl(a)

struct esSerialControl
{
	io_service * io;
	serial_port* port;

	void (*sendCommand)(struct esSerialControl*, int cmd, int type_arg, void * client);
	void * cliendSendCommand;
	void (*sendFreq)(struct esSerialControl*, int channel, int is_norm, int type_arg, void * client);
	void * cliendSendFreq;
	void (*sendADC)(struct esSerialControl*, int channel, int is_norm, int type_arg, void * client);
	void * cliendSendADC;

	void (*putCommand)(struct esSerialControl*, int cmd, void * buff, int type_arg, void * client);
	void * cliendPutCommand;
	void (*putFreq)(struct esSerialControl*, int channel, int is_norm, void * buff, int type_arg, void * client);
	void * cliendPutFreq;
	void (*putADC)(struct esSerialControl*, int channel, int is_norm, void * buff, int type_arg, void * client);
	void * cliendPutADC;

	char * err;

	char unsigned data_read[buff_size_max];
	int pos_read;
};

void sendCommandDefault(struct esSerialControl* control, int cmd, int type_arg, void * client)
{
	cerr << "operation \"get command\" - " << cmd << " not supported" << endl;
	esSSendError(control, 0);
}

void sendFreqDefault(struct esSerialControl* control, int channel, int is_norm, int type_arg, void * client)
{
	cerr << "operation \"get freq\" not supported" << endl;
	esSSendError(control, 0);
}

void sendADCDefault(struct esSerialControl* control, int channel, int is_norm, int type_arg, void * client)
{
	cerr << "operation \"get power\" not supported" << endl;
	esSSendError(control, 0);
}

void putCommandDefault(struct esSerialControl* control, int cmd, void * buff, int type_arg, void * client)
{
	cerr << "operation \"put command\" - " << cmd << " not supported" << endl;
	esSSendError(control, 0);
}

void putFreqDefault(struct esSerialControl* control, int channel, int is_norm, void * buff, int type_arg, void * client)
{
	cerr << "operation \"put turn\" not supported" << endl;
	esSSendError(control, 0);
}

void putADCDefault(struct esSerialControl* control, int channel, int is_norm, void * buff, int type_arg, void * client)
{
	cerr << "operation \"put power\" not supported" << endl;
	esSSendError(control, 0);
}

/*
 void putErrorDefault(struct esSerialControl* control, void * buff, int type_arg, void * client)
 {
 int data = -1;
 switch (type_arg)
 {
 case esSByte:
 data = *(uint8_t*) buff;
 break;
 case esSWord:
 data = *(uint16_t *) buff;
 break;
 case esSFloat:
 data = *(float *) buff;
 break;
 default:
 break;
 }

 if (data != 0)
 cerr << "receive Error - " << data << endl;
 }
 */

unsigned char esSCrc8(unsigned char *pcBlock, unsigned int length)
{
	size_t len = length;
	unsigned char crc = 0xFF;
	unsigned int i;

	while (len--)
	{
		crc ^= *pcBlock++;

		for (i = 0; i < 8; i++)
			crc = crc & 0x80 ? (crc << 1) ^ 0x31 : crc << 1;
	}

	return crc;
}

void esSSetErrorMsg(struct esSerialControl * control, const char * msg)
{
	delete[] control->err;

	size_t length = strlen(msg) + 1;
	control->err = new char[length];
	strcpy(control->err, msg);
	control->err[length - 1] = 0;
}

char * esSGetLastErrorMsg(struct esSerialControl * control)
{
	if (control->err == NULL)
		return "OK";
	else
		return control->err;
}

struct esSerialControl * esSInit()
{
	struct esSerialControl * retVal = new struct esSerialControl;
	memset(retVal, 0, sizeof(struct esSerialControl));

	try
	{
		retVal->io = new io_service();
	} catch (std::exception& e)
	{
		delete retVal->io;

		return NULL;
	}
	retVal->port = NULL;

	retVal->sendCommand = sendCommandDefault;
	retVal->sendFreq = sendFreqDefault;
	retVal->sendADC = sendADCDefault;

	retVal->putCommand = putCommandDefault;
	retVal->putFreq = putFreqDefault;
	retVal->putADC = putADCDefault;

	return retVal;
}

int esSOpen(struct esSerialControl * control, char * pathRS232)
{
	if (!control)
	{
		esSSetErrorMsg(control, "esSerialControl not initialized");
		return -ECANCELED;
	}

	if (!control->io)
	{
		esSSetErrorMsg(control, "esSerialControl->io not initialized");
		return -ECANCELED;
	}

	if (control->port)
	{
		esSSetErrorMsg(control, "esSerialControl->port already init");
		return -EBUSY;
	}

	try
	{
		control->port = new serial_port(*control->io, pathRS232);
	} catch (std::exception& e)
	{
		esSSetErrorMsg(control, e.what());

		delete control->port;
		control->port = NULL;

		return -ECANCELED;
	}

	return 0;
}

void esSClose(struct esSerialControl * control)
{
	if (control->port)
	{
		control->port->cancel();
		control->port->close();
	}

	delete control->port;
	control->port = NULL;
}

void esSClear(struct esSerialControl * control)
{
	esSClose(control);
	delete[] control->err;
	delete control->io;
	delete control;
}

int esSSetBaudRate(struct esSerialControl * control, uint32_t baud)
{
	if (!control)
	{
		esSSetErrorMsg(control, "esSerialControl not initialized");
		return -ECANCELED;
	}

	if (!control->port)
	{
		esSSetErrorMsg(control, "esSerialControl->port not initialized");
		return -ECANCELED;
	}

	try
	{
		serial_port_base::baud_rate BAUD(baud);
		control->port->set_option(BAUD);
	} catch (std::exception& e)
	{
		esSSetErrorMsg(control, e.what());

		return -ECANCELED;
	}
	return 0;
}

int esSSetCharSize(struct esSerialControl * control, int size)
{
	if (!control)
	{
		esSSetErrorMsg(control, "esSerialControl not initialized");
		return -ECANCELED;
	}

	if (!control->port)
	{
		esSSetErrorMsg(control, "esSerialControl->port not initialized");
		return -ECANCELED;
	}

	try
	{
		serial_port_base::character_size lCSIZE(size);
		control->port->set_option(lCSIZE);
	} catch (std::exception& e)
	{
		esSSetErrorMsg(control, e.what());

		return -ECANCELED;
	}
	return 0;
}

int esSSetFlow(struct esSerialControl * control, int flow)
{
	if (!control)
	{
		esSSetErrorMsg(control, "esSerialControl not initialized");
		return -ECANCELED;
	}

	if (!control->port)
	{
		esSSetErrorMsg(control, "esSerialControl->port not initialized");
		return -ECANCELED;
	}

	try
	{
		serial_port_base::flow_control::type type;
		switch (flow)
		{
		case esSFlowNone:
			type = serial_port_base::flow_control::none;
			break;
		case esSFlowSoftware:
			type = serial_port_base::flow_control::software;
			break;
		case esSFlowHardware:
			type = serial_port_base::flow_control::hardware;
			break;
		default:
			esSSetErrorMsg(control, "operation not permitted");
			return -EPERM;
		}

		serial_port_base::flow_control FLOW(type);
		control->port->set_option(FLOW);
	} catch (std::exception& e)
	{
		esSSetErrorMsg(control, e.what());

		return -ECANCELED;
	}

	return 0;
}

int esSSetParity(struct esSerialControl * control, int parity)
{
	if (!control)
	{
		esSSetErrorMsg(control, "esSerialControl not initialized");
		return -ECANCELED;
	}

	if (!control->port)
	{
		esSSetErrorMsg(control, "esSerialControl->port not initialized");
		return -ECANCELED;
	}

	try
	{
		serial_port_base::parity::type type;
		switch (parity)
		{
		case esSParityNone:
			type = serial_port_base::parity::none;
			break;
		case esSParityOdd:
			type = serial_port_base::parity::odd;
			break;
		case esSParityEven:
			type = serial_port_base::parity::even;
			break;
		default:
			esSSetErrorMsg(control, "operation not permitted");
			return -EPERM;
		}

		serial_port_base::parity PARITY(type);
		control->port->set_option(PARITY);
	} catch (std::exception& e)
	{
		esSSetErrorMsg(control, e.what());

		return -ECANCELED;
	}

	return 0;
}

int esSSetStop(struct esSerialControl * control, int stop)
{
	if (!control)
	{
		esSSetErrorMsg(control, "esSerialControl not initialized");
		return -ECANCELED;
	}

	if (!control->port)
	{
		esSSetErrorMsg(control, "esSerialControl->port not initialized");
		return -ECANCELED;
	}

	try
	{
		serial_port_base::stop_bits::type type;
		switch (stop)
		{
		case esSStopOne:
			type = serial_port_base::stop_bits::one;
			break;
		case esSStopTwo:
			type = serial_port_base::stop_bits::two;
			break;
		case esSStopOnePointFive:
			type = serial_port_base::stop_bits::onepointfive;
			break;
		default:
			esSSetErrorMsg(control, "operation not permitted");
			return -EPERM;
		}

		serial_port_base::stop_bits STOP(type);
		control->port->set_option(STOP);
	} catch (std::exception& e)
	{
		esSSetErrorMsg(control, e.what());

		return -ECANCELED;
	}

	return 0;
}

void esRSAsyncHandler(size_t & sizeRead, const boost::system::error_code& error, size_t bytes_transferred)
{
	sizeRead = bytes_transferred;
}

void esRSWaitHandler(serial_port& port, const boost::system::error_code& error)
{
	if (error)
		return;

	port.cancel();
}

int esSGetData(struct esSerialControl * control, unsigned char * buff, unsigned int size, int timeout)
{
	size_t sizeRead = 0;

	if (!control)
	{
		esSSetErrorMsg(control, "esSerialControl not initialized");
		return -ECANCELED;
	}

	if (!control->io)
	{
		esSSetErrorMsg(control, "esSerialControl->io not initialized");
		return -ECANCELED;
	}

	if (!control->port)
	{
		esSSetErrorMsg(control, "esSerialControl->port not initialized");
		return -ECANCELED;
	}

	try
	{
		if (timeout == 0)
		{
			sizeRead = control->port->read_some(buffer(buff, size));
			return (int) sizeRead;
		}

		if (!control->io)
		{
			return -ECANCELED;
		}

		control->io->reset();

		deadline_timer timeoutTimer(*control->io);
		timeoutTimer.expires_from_now(boost::posix_time::milliseconds(timeout));

		control->port->async_read_some(buffer(buff, size),
				boost::bind(&esRSAsyncHandler, boost::ref(sizeRead), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
		timeoutTimer.async_wait(
				boost::bind(&esRSWaitHandler, boost::ref(*control->port), boost::asio::placeholders::error));
		control->io->run();

	} catch (std::exception& e)
	{
		esSSetErrorMsg(control, e.what());

		return -ECANCELED;
	}

	return (int) sizeRead;
}

int esSSendGetData(struct esSerialControl * control, char command, int type, int withCRC8)
{
	if (!control)
	{
		esSSetErrorMsg(control, "esSerialControl not initialized");
		return -ECANCELED;
	}

	if (!control->port)
	{
		esSSetErrorMsg(control, "esSerialControl->port not initialized");
		return -ECANCELED;
	}

	const unsigned char bitsGet = ((withCRC8 ? esSEnableCRC : esSDisableCRC) << esSBitsPosCRC)
			| (esSGet << esSBitsPosType) | (type << esSBitsPosLength);

	const unsigned char codeGet = (command << esSCodePosNum);
	unsigned char commandGet[] =
	{ esSFrameStart, bitsGet, codeGet, esSFrameStop, esSFrameInterlive };

	if (withCRC8)
		commandGet[4] = esSCrc8(commandGet + 1, 2);

	try
	{
		control->port->write_some(buffer(commandGet, 5));
	} catch (std::exception& e)
	{
		esSSetErrorMsg(control, e.what());

		return -ECANCELED;
	}

	return 0;
}

int esSSendGetFreq(struct esSerialControl * control, int channel, char is_norm, int type, int withCRC8)
{
	const char command = (esSFreq << esSCodePosFreq) | char(channel & esSCodeMaskNum)
			| ((is_norm ? esSNorm : esSNotNorm) << esSCodePosNorm);
	return esSSendGetData(control, command, type, withCRC8);
}

int esSSendGetADC(struct esSerialControl * control, int channel, char is_norm, int type, int withCRC8)
{
	const char command = (esSADC << esSCodePosFreq) | char(channel & esSCodeMaskNum)
			| ((is_norm ? esSNorm : esSNotNorm) << esSCodePosNorm);
	return esSSendGetData(control, command, type, withCRC8);
}
int esSSendCMDGet(struct esSerialControl * control, char command, int type, int withCRC8)
{
	return esSSendGetData(control, command | esSCodeMaskCommand, type, withCRC8);
}

int esSSendPutData(struct esSerialControl * control, char command, void * data, size_t lendata, int withCRC8)
{
	unsigned char type;
	unsigned char datanw[4];

	switch (lendata)
	{
	case 0:
		type = esSZero;
		break;
	case 1:
		type = esSByte;
		*datanw = *(unsigned char*) data;
		break;
	case 2:
		type = esSWord;
		*((uint16_t*) datanw) = htons(*(uint16_t*) data);
		break;
	case 4:
		type = esSFloat;
		*((uint32_t*) datanw) = htonl(*(uint32_t*) data);
		break;
	default:
		esSSetErrorMsg(control, "Length not supported");
		return -1;
	}

	const unsigned char bitsGet = ((withCRC8 ? esSEnableCRC : esSDisableCRC) << esSBitsPosCRC)
			| (esSPut << esSBitsPosType) | (type << esSBitsPosLength);
	const unsigned char codeGet = (command << esSCodePosNum);

	const size_t size = esSMinLengthPacket + lendata;
	unsigned char commandPut[esSMaxLengthPacket];
	commandPut[0] = esSFrameStart;
	commandPut[1] = bitsGet;
	commandPut[2] = codeGet;
	memcpy(commandPut + 3, datanw, lendata);

	commandPut[size - 2] = esSFrameStop;
	if (withCRC8)
		commandPut[size - 1] = esSCrc8(commandPut + 1, size - 3);
	else
		commandPut[size - 1] = esSFrameInterlive;

	try
	{
		control->port->write_some(boost::asio::buffer(commandPut, size));
	} catch (std::exception& e)
	{
		esSSetErrorMsg(control, e.what());

		return -ECANCELED;
	}

	return 0;
}

int esSSendError(struct esSerialControl * control, short err_no, int withCRC8)
{
	return esSSendPutData(control, char(esSCodeError), &err_no, sizeof(short), withCRC8);
}

int esSSendEnableFreq(struct esSerialControl * control, uint8_t channel, bool enable, int withCRC8)
{
	const char command = (1 << esSCodePosCommand) | (enable ? esCMD_ENABLE_FREQ_CH : esCMD_DISABLE_FREQ_CH);
	return esSSendPutData(control, command, &channel, sizeof(channel), withCRC8);
}

int esSSendFreq(struct esSerialControl * control, int channel, void * data, size_t lendata, char is_norm, int withCRC8)
{
	const char command = (esSFreq << esSCodePosFreq) | char(channel && esSCodeMaskNum)
			| ((is_norm ? esSNorm : esSNotNorm) << esSCodePosNorm);
	return esSSendPutData(control, command, data, lendata, withCRC8);
}

int esSSendFreqByte(struct esSerialControl * control, int channel, char is_norm, char freq, int withCRC8)
{
	return esSSendFreq(control, channel, &freq, sizeof(freq), is_norm, withCRC8);
}
int esSSendFreqWord(struct esSerialControl * control, int channel, int16_t freq, char is_norm, int withCRC8)
{
	return esSSendFreq(control, channel, &freq, sizeof(freq), is_norm, withCRC8);
}
int esSSendFreqFloat(struct esSerialControl * control, int channel, float freq, char is_norm, int withCRC8)
{
	return esSSendFreq(control, channel, &freq, sizeof(freq), is_norm, withCRC8);
}

int esSSendEnableADC(struct esSerialControl * control, uint8_t channel, bool enable, int withCRC8)
{
	const char command = (1 << esSCodePosCommand) | (enable ? esCMD_ENABLE_ADC_CH : esCMD_DISABLE_ADC_CH);
	return esSSendPutData(control, command, &channel, sizeof(channel), withCRC8);
}

int esSSendADC(struct esSerialControl * control, int channel, void * data, size_t lendata, char is_norm, int withCRC8)
{
	const char command = (esSADC << esSCodePosFreq) | char(channel && esSCodeMaskNum)
			| ((is_norm ? esSNorm : esSNotNorm) << esSCodePosNorm);
	return esSSendPutData(control, command, data, lendata, withCRC8);
}
int esSSendADCByte(struct esSerialControl * control, int channel, char vol, char is_norm, int withCRC8)
{
	return esSSendADC(control, channel, &vol, sizeof(vol), is_norm, withCRC8);
}
int esSSendADCWord(struct esSerialControl * control, int channel, int16_t vol, char is_norm, int withCRC8)
{
	return esSSendADC(control, channel, &vol, sizeof(vol), is_norm, withCRC8);
}
int esSSendADCFloat(struct esSerialControl * control, int channel, int16_t vol, char is_norm, int withCRC8)
{
	return esSSendADC(control, channel, &vol, sizeof(vol), is_norm, withCRC8);
}
int esSSendCMDByte(struct esSerialControl * control, char command, char data, int withCRC8)
{
	return esSSendPutData(control, command | esSCodeMaskCommand, &data, sizeof(char), withCRC8);
}
int esSSendCMDWord(struct esSerialControl * control, char command, short data, int withCRC8)
{
	return esSSendPutData(control, command | esSCodeMaskCommand, &data, sizeof(short), withCRC8);
}
int esSSendCMDFloat(struct esSerialControl * control, char command, float data, int withCRC8)
{
	return esSSendPutData(control, command | esSCodeMaskCommand, &data, sizeof(float), withCRC8);
}

int esSParse(struct esSerialControl * control, unsigned char * buff, unsigned int size)
{
	if (size == 0)
		return 0;

	if (buff[0] != esSFrameStart)
		return 1;

	if (size < esSMinLengthPacket)
		return 0;

	const int put = (buff[1] & esSBitsMaskType) >> esSBitsPosType;
	const size_t type_arg = (buff[1] & esSBitsMaskLength) >> esSBitsPosLength;

	size_t lengthData = 0;
	if (put)
		switch (type_arg)
		{
		case esSZero:
			lengthData = 0;
			break;
		case esSByte:
			lengthData = 1;
			break;
		case esSWord:
			lengthData = 2;
			break;
		case esSFloat:
			lengthData = 4;
			break;
		}

	size_t length = lengthData + esSMinLengthPacket;
	if (length > size)
		return 0;

	if (buff[length - 2] != esSFrameStop)
		return length;

	if ((buff[1] & esSBitsMaskCRC) >> esSBitsPosCRC)
	{
		if (esSCrc8(buff + 1, length - 3) != buff[length - 1])
			return length;
	}
	else
	{
		if (buff[length - 1] != 0)
			return length;
	}

	const int isCMD = (buff[2] & esSCodeMaskCommand) >> esSCodePosCommand;
	const int vol = (buff[2] & (isCMD ? esSCodeMaskCode : esSCodeMaskNum)) >> esSCodePosNum;
	unsigned char data_out[4];
	switch (lengthData)
	{
	case 1:
		*data_out = *(buff + 3);
		break;
	case 2:
		*(uint16_t*) data_out = htons(*((uint16_t*) (buff + 3)));
		break;
	case 4:
#ifdef __VFP_FP__
		*(uint32_t*) data_out = htonl_g(*((uint32_t*) (buff + 3)));
#else
		*(uint32_t*) data_out = htonl(*((uint32_t*) (buff + 3)));
#endif
		break;
	}
	if (isCMD)
	{
		if (put)
			control->putCommand(control, vol, data_out, type_arg, control->cliendPutCommand);
		else
			control->sendCommand(control, vol, type_arg, control->cliendSendCommand);
	}
	else
	{
		const int isFreq = (buff[2] & esSCodeMaskADCFreq) >> esSCodePosFreq;
		const int isNorm = (buff[2] & esSCodeMaskNorm) >> esSCodePosNorm;

		if (put)
		{
			if (isFreq == esSFreq)
				control->putFreq(control, vol, isNorm, data_out, type_arg, control->cliendPutFreq);
			else
				control->putADC(control, vol, isNorm, data_out, type_arg, control->cliendPutADC);

		}
		else
		{
			if (isFreq == esSFreq)
				control->sendFreq(control, vol, isNorm, type_arg, control->cliendSendFreq);
			else
				control->sendADC(control, vol, isNorm, type_arg, control->cliendSendADC);

		}
	}

	return length;
}

int esSUpdate(struct esSerialControl * control, unsigned int max_time)
{
	int length = 0;
	length = esSGetData(control, control->data_read, 256 - control->pos_read, max_time);

	if (length < 0)
	{
		return -ECANCELED;
	}
	else if (length == 0)
		return 0;

	control->pos_read += length;

	int needDel;
	while ((needDel = esSParse(control, control->data_read, control->pos_read)) != 0)
	{
		if (needDel >= control->pos_read)
			control->pos_read = 0;
		else
		{
			memcpy(control->data_read, control->data_read + needDel,
					(control->pos_read - needDel) * sizeof(char unsigned));
			control->pos_read = control->pos_read - needDel;
		}
	}

	if (control->pos_read == 256)
		control->pos_read = 0;

	return 1;
}

void esSetCallBackSendCommand(struct esSerialControl * control, void (*send)(struct esSerialControl*, int, int, void *),
		void * client)
{
	control->sendCommand = send;
	control->cliendSendCommand = client;
}
void esSetCallBackSendFreq(struct esSerialControl * control,
		void (*send)(struct esSerialControl*, int channel, int is_norm, int type_arg, void * client), void * client)
{
	control->sendFreq = send;
	control->cliendSendFreq = client;
}
void esSetCallBackSendADC(struct esSerialControl * control,
		void (*send)(struct esSerialControl*, int channel, int is_norm, int type_arg, void * client), void * client)
{
	control->sendADC = send;
	control->cliendSendADC = client;
}

void esSetCallBackReceiveCommand(struct esSerialControl * control,
		void (*put)(struct esSerialControl*, int, void *, int, void *), void * client)
{
	control->putCommand = put;
	control->cliendPutCommand = client;
}
void esSetCallBackReceiveFreq(struct esSerialControl * control,
		void (*put)(struct esSerialControl*, int channel, int is_norm, void * buff, int type_arg, void * client),
		void * client)
{
	control->putFreq = put;
	control->cliendPutFreq = client;
}
void esSetCallBackReceiveADC(struct esSerialControl * control,
		void (*put)(struct esSerialControl*, int channel, int is_norm, void * buff, int type_arg, void * client),
		void * client)
{
	control->putADC = put;
	control->cliendPutADC = client;
}
